package com.teamterry.administrationsystem.services;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.PermissionRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class PermissionService {

    @Inject
    @JPA
    private PermissionRepository permissionRepository;

    /**
     * Get all Permissions
     * @return a response with either an error or with the list of permissions
     */
    public ObjectResponse<List<Permission>> all() {
        List<Permission> permissions = permissionRepository.findAll();

        return new ObjectResponse<>(HttpCode.OK, String.format("%s permissions found", permissions.size()), permissions);
    }

    /**
     * Find a permission by it's ID
     * @param id - the id of the permission you are trying to find
     * @return a response with either an error or with the permission that has been found
     */
    public ObjectResponse<Permission> findById(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Permission with id %s is invalid", id));
        }

        Permission permission = permissionRepository.findOne(id);

        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Permission with id %s not found", id));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Permission with id %s and name %s found", id, permission.getName()), permission);
    }

    /**
     * Find a permission by it's name
     * @param name- the name of the permission you are trying to find
     * @return a response with either an error or with the permission that has been found
     */
    public ObjectResponse<Permission> findByName(String name) {
        if(name.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission name can not be empty");
        }

        Permission permission = permissionRepository.findByName(name);

        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Permission with name %s not found", name));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Permission with id %s and name %s found", permission.getId(), name), permission);
    }

    /**
     * Create a Permission
     * @param permission - the permission information
     * @return a response with either an error or with the permission that has been created
     */
    public ObjectResponse<Permission> create(Permission permission) {
        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission can not be null");
        }

        if(permission.getName().isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission nam can not be empty");
        }

        ObjectResponse<Permission> getByNameResponse = findByName(permission.getName());

        if(getByNameResponse.getObject() != null) {
            return new ObjectResponse<>(HttpCode.CONFLICT, String.format("Permission with name %s already exists", permission.getName()));
        }

        Permission result = permissionRepository.save(permission);

        if(result != null) {
            return new ObjectResponse<>(HttpCode.CREATED, String.format("A Permission with the name %s has been created", result.getName()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Permission could not be created due to an unknown error");
        }
    }

    /**
     * Update a Permission
     * @param permission - the permission information
     * @return a response with either an error or with the permission that has been updated
     */
    public ObjectResponse<Permission> update(Permission permission) {
        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission can not be null");
        }

        if(permission.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Permission with id %s is invalid", permission.getId()));
        }

        if(permission.getName().isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission nam can not be empty");
        }

        ObjectResponse<Permission> getByIdResponse = findById(permission.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        ObjectResponse<Permission> getByNameResponse = findByName(permission.getName());

        if(getByNameResponse.getObject() != null && permission.getId() != getByNameResponse.getObject().getId()) {
            return new ObjectResponse<>(HttpCode.CONFLICT, String.format("Permission with name %s already exists", permission.getName()));
        }

        Permission result = permissionRepository.save(permission);
        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("A Permission with the id %s has been updated", result.getId()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Permission could not be updated due to an unknown error");
        }
    }

    /**
     * Delete an existing permission
     * @param permission - the permission to be deleted
     * @return a response with either an error or a message which states that it has been deleted
     */
    public ObjectResponse<Permission> delete(Permission permission) {
        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission can not be null");
        }

        if(permission.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Permission with id %s is invalid", permission.getId()));
        }

        ObjectResponse<Permission> getByIdResponse = findById(permission.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        permissionRepository.delete(permission);

        return new ObjectResponse<>(HttpCode.OK, String.format("Permission with name %s has been deleted", permission.getName()));
    }
}
