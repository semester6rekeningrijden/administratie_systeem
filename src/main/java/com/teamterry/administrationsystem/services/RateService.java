package com.teamterry.administrationsystem.services;

import com.teamterry.administrationsystem.domain.Rate;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.RateRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class RateService {

    @Inject
    @JPA
    private RateRepository rateRepository;

    /**
     * Get all Rates
     * @return
     */
    public ObjectResponse<List<Rate>> all() {
        List<Rate> result = rateRepository.findAll();

        return new ObjectResponse<>(HttpCode.OK, String.format("%s rates found", result.size()), result);
    }

    /**
     * Find a Rate by its ID
     * @param id
     * @return
     */
    public ObjectResponse<Rate> findById(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Rate with id %s is invalid", id));
        }

        Rate result = rateRepository.findOne(id);

        if(result == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Rate with id %s not found", id));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("rate with id %s and price %s found", id, result.getPrice()), result);
    }

    /**
     * Create a new Rate
     * @param rate
     * @return
     */
    public ObjectResponse<Rate> create(Rate rate) {
        if(rate == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Rate can not be null");
        }

        if(rate.getPrice() < 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "The Rate price can not be negative");
        }

        if (rate.getStartDate() == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Startdate can not be null");
        }

        if(rate.getEndDate() == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Enddate can not be null");
        }

        if(rate.getStartDate().isAfter(rate.getEndDate())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Startdate can not be later then the Enddate");
        }

        if(rate.getEndDate().isBefore(rate.getStartDate())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Enddate can not be earlier then the Startdate");
        }

        Rate result = rateRepository.save(rate);

        if(result != null) {
            return new ObjectResponse<>(HttpCode.CREATED, String.format("A Rate with the price %s has been created", result.getPrice()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Rate could not be created due to an unknown error");
        }
    }

    /**
     * Update a Rate
     * @param rate
     * @return
     */
    public ObjectResponse<Rate> update(Rate rate) {
        if(rate == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Rate can not be null");
        }

        if(rate.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Rate with id %s is invalid", rate.getId()));
        }

        if(rate.getPrice() < 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "The Rate price can not be negative");
        }

        if (rate.getStartDate() == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Startdate can not be null");
        }

        if(rate.getEndDate() == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Enddate can not be null");
        }

        if(rate.getStartDate().isAfter(rate.getEndDate())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Startdate can not be later then the Enddate");
        }

        if(rate.getEndDate().isBefore(rate.getStartDate())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Enddate can not be earlier then the Startdate");
        }

        ObjectResponse<Rate> findByIdResponse = findById(rate.getId());

        if(findByIdResponse.getObject() == null) {
            return findByIdResponse;
        }

        Rate result = rateRepository.save(rate);

        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("A Rate with the id %s has been updated", result.getId()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Rate could not be updated due to an unknown error");
        }
    }

    /**
     * Delete a rate
     * @param id
     * @return
     */
    public ObjectResponse<Rate> delete(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Rate with id %s is invalid", id));
        }

        ObjectResponse<Rate> findByIdResponse = findById(id);

        if(findByIdResponse.getObject() == null) {
            return findByIdResponse;
        }

        rateRepository.delete(findByIdResponse.getObject().getId());

        return new ObjectResponse<>(HttpCode.OK, String.format("Rate with id %s has been deleted", id));
    }
}
