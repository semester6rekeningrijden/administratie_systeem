package com.teamterry.administrationsystem.services;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.repositories.interfaces.InvoiceRepository;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class InvoiceService {

    @Inject
    @JPA
    private InvoiceRepository invoiceRepository;

    public ObjectResponse<List<Invoice>> getAll() {
        List<Invoice> invoices = invoiceRepository.findAll();

        if(invoices == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "No existing invoices");
        }

        if(invoices.size() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "No existing invoices");
        }

        return new ObjectResponse<>(HttpCode.OK, "All invoices", invoices);
    }

    public ObjectResponse<List<Invoice>> getAllFromUser(User u){
        if(u == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "User is null");
        }

        if(u.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("User with id %s is invalid", u.getId()));
        }

        List<Invoice> invoices = invoiceRepository.getAllFromUser(u);

        if(invoices == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Invoices not found for user with id %s", u.getId()));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Invoices with user with id %s", u.getId()), invoices);
    }

    public ObjectResponse<Invoice> getFromID(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Id %s is invalid", id));
        }

        Invoice invoice = invoiceRepository.findOne(id);

        if(invoice == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Invoice with id %s not found", id));
            }

        return new ObjectResponse<>(HttpCode.OK, String.format("Permission with id %s", id), invoice);
    }

    public ObjectResponse<Invoice> addInvoice(Invoice invoice) {
        if(invoice == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invoice is null");
        }

        if(invoice.getVehicleTrackerId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with VehicleTrackerId %s is invalid", invoice.getVehicleTrackerId()));
        }

        if(invoice.getInvoiceNumber() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with InvoiceNumber %s is invalid", invoice.getInvoiceNumber()));
        }

        Invoice result = invoiceRepository.save(invoice);

        if(result == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "Failed to create invoice");
        }

        return new ObjectResponse<>(HttpCode.OK, "New invoice created", result);
    }

    public ObjectResponse<Invoice> editInvoice(Invoice invoice) {
        if(invoice == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invoice is null");
        }

        if(invoice.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with id %s is invalid", invoice.getId()));
        }

        if(invoice.getVehicleTrackerId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with VehicleTrackerId %s is invalid", invoice.getVehicleTrackerId()));
        }

        if(invoice.getInvoiceNumber() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with InvoiceNumber %s is invalid", invoice.getInvoiceNumber()));
        }

        Invoice result = invoiceRepository.save(invoice);

        if(result == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "Failed to edit invoice");
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Invoice with id %s edited", invoice.getId()), result);
    }

    public ObjectResponse<Boolean> removeInvoice(Invoice invoice) {
        if(invoice == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invoice is null");
        }

        if(invoice.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with id %s is invalid", invoice.getId()));
        }

        invoiceRepository.delete(invoice);

        return new ObjectResponse<>(HttpCode.OK, String.format("Invoice with id %s removed", invoice.getId()), true);
    }
}
