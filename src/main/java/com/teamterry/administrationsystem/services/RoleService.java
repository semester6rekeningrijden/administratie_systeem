package com.teamterry.administrationsystem.services;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.RoleRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class RoleService {

    @Inject
    @JPA
    private RoleRepository roleRepository;

    @Inject
    private PermissionService permissionService;

    /**
     * Get all Roles
     * @return a response with either an error or with the list of roles
     */
    public ObjectResponse<List<Role>> all() {
        List<Role> roles = roleRepository.findAll();

        return new ObjectResponse<>(HttpCode.OK, String.format("%s roles found", roles.size()), roles);
    }

    /**
     * Find a role by it's ID
     * @param id - the id of the role you are trying to find
     * @return a response with either an error or with the role that has been found
     */
    public ObjectResponse<Role> findById(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Role with id %s is invalid", id));
        }

        Role role = roleRepository.findOne(id);

        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Role with id %s not found", id));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Role with id %s and name %s found", id, role.getName()), role);
    }

    /**
     * Find a role by it's name
     * @param name- the name of the role you are trying to find
     * @return a response with either an error or with the role that has been found
     */
    public ObjectResponse<Role> findByName(String name) {
        if(name.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role name can not be empty");
        }

        Role role = roleRepository.findByName(name);

        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Role with name %s not found", name));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Role with id %s and name %s found", role.getId(), name), role);
    }

    /**
     * Create a Role
     * @param role - the role information
     * @return a response with either an error or with the role that has been created
     */
    public ObjectResponse<Role> create(Role role) {
        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role can not be null");
        }

        if(role.getName().isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role name can not be empty");
        }

        ObjectResponse<Role> getByNameResponse = findByName(role.getName());

        if(getByNameResponse.getObject() != null) {
            return new ObjectResponse<>(HttpCode.CONFLICT, String.format("Role with name %s already exists", role.getName()));
        }

        Role result = roleRepository.save(role);

        if(result != null) {
            return new ObjectResponse<>(HttpCode.CREATED, String.format("A Role with the name %s has been created", result.getName()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Role could not be created due to an unknown error");
        }
    }

    /**
     * Update a Role
     * @param role - the role information
     * @return a response with either an error or with the role that has been updated
     */
    public ObjectResponse<Role> update(Role role) {
        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role can not be null");
        }

        if(role.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Role with id %s is invalid", role.getId()));
        }

        if(role.getName().isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role nam can not be empty");
        }

        ObjectResponse<Role> getByIdResponse = findById(role.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        ObjectResponse<Role> getByNameResponse = findByName(role.getName());

        if(getByNameResponse.getObject() != null && role.getId() != getByNameResponse.getObject().getId()) {
            return new ObjectResponse<>(HttpCode.CONFLICT, String.format("Role with name %s already exists", role.getName()));
        }

        Role result = roleRepository.save(role);
        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("A Role with the id %s has been updated", result.getId()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Role could not be updated due to an unknown error");
        }
    }

    /**
     * Delete an existing role
     * @param role - the role to be deleted
     * @return a response with either an error or a message which states that it has been deleted
     */
    public ObjectResponse<Role> delete(Role role) {
        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role can not be null");
        }

        if(role.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Role with id %s is invalid", role.getId()));
        }

        ObjectResponse<Role> getByIdResponse = findById(role.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        roleRepository.delete(role);

        return new ObjectResponse<>(HttpCode.OK, String.format("Role with name %s has been deleted", role.getName()));
    }

    /**
     * Attach a permission to a role
     * @param role - the role
     * @param permission - the permission
     * @return - a response with either an error or with the role with the newly added permission
     */
    public ObjectResponse<Role> attachPermission(Role role, Permission permission) {
        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role can not be null");
        }

        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission can not be null");
        }

        if(role.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Role with id %s is invalid", role.getId()));
        }

        if(permission.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Permission with id %s is invalid", permission.getId()));
        }

        ObjectResponse<Permission> getPermissionByIdResponse = permissionService.findById(permission.getId());

        if(getPermissionByIdResponse.getObject() == null) {
            return new ObjectResponse<>(getPermissionByIdResponse.getCode(), getPermissionByIdResponse.getMessage());
        }

        ObjectResponse<Role> getByIdResponse = findById(role.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        if(getByIdResponse.getObject().getPermissions().contains(permission)) {
            return new ObjectResponse<>(HttpCode.FORBIDDEN, String.format("Role: %s already has the permission: %s", role.getName(), permission.getName()));
        }

        Role result = roleRepository.attachPermission(role, permission);

        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("Permission: %s has been attached to Role: %s", permission.getName(), role.getName()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, String.format("Permission: %s could not be attached to Role: %s due to an unknown error", permission.getName(), role.getName()));
        }
    }

    /**
     * Detach a permission from a role
     * @param role - the role
     * @param permission - the permission
     * @return - a response with either an error or with the role without the detached permission
     */
    public ObjectResponse<Role> detachPermission(Role role, Permission permission) {
        if(role == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Role can not be null");
        }

        if(permission == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Permission can not be null");
        }

        if(role.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Role with id %s is invalid", role.getId()));
        }

        if(permission.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Permission with id %s is invalid", permission.getId()));
        }

        ObjectResponse<Permission> getPermissionByIdResponse = permissionService.findById(permission.getId());

        if(getPermissionByIdResponse.getObject() == null) {
            return new ObjectResponse<>(getPermissionByIdResponse.getCode(), getPermissionByIdResponse.getMessage());
        }

        ObjectResponse<Role> getByIdResponse = findById(role.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        if(!getByIdResponse.getObject().getPermissions().contains(permission)) {
            return new ObjectResponse<>(HttpCode.FORBIDDEN, String.format("Role: %s does not have the permission: %s", role.getName(), permission.getName()));
        }

        Role result = roleRepository.detachPermission(role, permission);

        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("Permission: %s has been detached from Role: %s", permission.getName(), role.getName()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, String.format("Permission: %s could not be detached from Role: %s due to an unknown error", permission.getName(), role.getName()));
        }
    }
}
