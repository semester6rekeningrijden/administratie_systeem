package com.teamterry.administrationsystem.services;

import com.google.common.collect.Iterables;
import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.UserRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;

import com.teamterry.administrationsystem.utils.PasswordAuthentication;
import com.teamterry.administrationsystem.utils.Util;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class UserService {

    @Inject
    @JPA
    private UserRepository userRepository;

    @Inject
    private RoleService roleService;


    /**
     * Get all Users
     * @return a list of users
     */
    public ObjectResponse<List<User>> all() {
        List<User> users = userRepository.findAll();
        return new ObjectResponse<>(HttpCode.OK, users.size() + " users loaded", users);
    }

    /**
     * Get a user by its id
     * @param id - the id of the user
     * @return a user
     */
    public ObjectResponse<User> getById(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invalid ID");
        }

        User user = userRepository.findOne(id);

        if(user == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "User not found");
        }

        return new ObjectResponse<>(HttpCode.OK, "User with email: " + user.getEmail() + " found", user);
    }

    /**
     * Get a user by its email
     * @param email - the email of the user
     * @return a user
     */
    public ObjectResponse<User> getByEmail(String email) {
        if(email.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "email can not be empty");
        }

        User user = userRepository.findByEmail(email);

        if(user == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "User not found");
        }

        return new ObjectResponse<>(HttpCode.OK, "User with username: " + user.getEmail() + " found", user);
    }

    /**
     * Get a list of users by zipcode
     * @param zipcode - the zipcode of the users
     * @return a list of users
     */
    public ObjectResponse<List<User>> getByZipcode(String zipcode) {
        if(zipcode.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "zipcode can not be empty");
        }

        List<User> users = userRepository.findByZipcode(zipcode);

        if(users == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "No Users found on this zipcode");
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("%s users found on zipcode %s", users.size(), zipcode), users);
    }

    /**
     * Get a list of users by name
     * @param firstname - the firstname of the user
     * @param lastname - the lastname of the user
     * @return a list of users
     */
    public ObjectResponse<List<User>> getByName(String firstname, String lastname) {
        if(firstname.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "firstname can not be empty");
        }

        if(lastname.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "lastname can not be empty");
        }

        List<User> users = userRepository.findByName(firstname, lastname);

        if(users == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "No Users found with this name");
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("%s users found with name %s %s", users.size(), firstname, lastname), users);
    }

    /**
     * Create a new user
     * @param user - the user information
     * @return the newly created user
     */
    public ObjectResponse<User> create(User user) {
        if(user == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "user can not be NULL");
        }

        if(Util.isNullorEmpty(user.getEmail())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "email can not be empty");
        }

        if(Util.isNullorEmpty(user.getFirstName())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "firstname can not be empty");
        }

        if(Util.isNullorEmpty(user.getLastName())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "lastname can not be empty");
        }

        if(Util.isNullorEmpty(user.getCity())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "city can not be empty");
        }

        if(Util.isNullorEmpty(user.getZipcode())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "zipcode can not be empty");
        }

        if(Util.isNullorEmpty(user.getCountry())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "country can not be empty");
        }

        ObjectResponse<User> getByEmailResponse = getByEmail(user.getEmail());
        if(getByEmailResponse.getObject() != null) {
            return new ObjectResponse<>(HttpCode.CONFLICT, String.format("User with email %s already exists", user.getEmail()));
        }

        ObjectResponse<Role> getRoleByNameResponse = roleService.findByName("employee");

        if(getRoleByNameResponse.getObject() == null) {
            return new ObjectResponse<>(getRoleByNameResponse.getCode(), getRoleByNameResponse.getMessage());
        }

        user.setRole(getRoleByNameResponse.getObject());
        user.setPassword(PasswordAuthentication.hash(user.getPassword()));
        User created = userRepository.save(user);

        if(created != null) {
            return new ObjectResponse<>(HttpCode.CREATED, String.format("User with email %s has been created", created.getEmail()), created);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Could not create a new user due to an unknown error");
        }
    }

    /**
     * Update an existing user
     * @param user - the new user information with an existing user id
     * @return the updated user
     */
    public ObjectResponse<User> update(User user) {
        if(user.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invalid ID");
        }

        if(Util.isNullorEmpty(user.getEmail())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "email can not be empty");
        }

        if(Util.isNullorEmpty(user.getFirstName())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "firstname can not be empty");
        }

        if(Util.isNullorEmpty(user.getLastName())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "lastname can not be empty");
        }

        if(Util.isNullorEmpty(user.getCity())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "city can not be empty");
        }

        if(Util.isNullorEmpty(user.getZipcode())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "zipcode can not be empty");
        }

        if(Util.isNullorEmpty(user.getCountry())) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "country can not be empty");
        }

        ObjectResponse<User> getByIdResponse = getById(user.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        ObjectResponse<User> getByEmailResponse = getByEmail(user.getEmail());

        if(getByEmailResponse.getObject() != null && getByEmailResponse.getObject().getId() != user.getId()) {
            return new ObjectResponse<>(HttpCode.CONFLICT, String.format("User with email %s already exists", user.getEmail()));
        }

        User result = userRepository.save(user);
        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("User with email %s has been updated", user.getEmail()), user);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Could not update an existing user due to an unknown error");
        }
    }

    /**
     * Delete an existing user
     * @param user - the user to be deleted
     * @return a boolean wether or not the user is deleted.
     */
    public ObjectResponse<User> delete(User user) {
        if(user.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invalid ID");
        }

        ObjectResponse<User> getByIdResponse = getById(user.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        userRepository.delete(user);
        return new ObjectResponse<>(HttpCode.OK, String.format("User with email %s has been deleted", user.getEmail()));
    }

    /**
     * Login as a user
     * @param email - the username if the user
     * @param password - the password of the user
     * @return a User
     */
    public ObjectResponse<User> login(String email, String password) {
        if(email.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Username can not be empty");
        }

        if (password.isEmpty()) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Password can not be empty");
        }

        ObjectResponse<User> findByEmailResponse = getByEmail(email);

        if(findByEmailResponse.getObject() != null) {

            if(PasswordAuthentication.verify(password, findByEmailResponse.getObject().getPassword())) {
                return new ObjectResponse<>(HttpCode.OK, String.format("You are logged in as %s", findByEmailResponse.getObject().getEmail()), findByEmailResponse.getObject());
            }
        }

        return new ObjectResponse<>(HttpCode.UNAUTHORIZED, "Wrong username or password");
    }

    /**
     * Change the role of a user
     * @param user - the user
     * @param role - the role you want to add
     * @return the user
     */
    public ObjectResponse<User> changeRole(User user, Role role) {
        if(user.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invalid User ID");
        }

        if(role.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invalid Role ID");
        }

        ObjectResponse<User> getByIdResponse = getById(user.getId());

        if(getByIdResponse.getObject() == null) {
            return getByIdResponse;
        }

        ObjectResponse<Role> getRoleByIdResponse = roleService.findById(role.getId());
        if(getRoleByIdResponse.getObject() == null) {
            return new ObjectResponse<>(getRoleByIdResponse.getCode(), getRoleByIdResponse.getMessage());
        }

        user.setRole(role);
        User result =  userRepository.save(user);
        if(result != null) {
            return new ObjectResponse<>(HttpCode.OK, String.format("User with email %s now has role %s", user.getEmail(), role.getName()), result);
        } else {
            return new ObjectResponse<>(HttpCode.INTERNAL_SERVER_ERROR, "Could not add role to a User due to an unknown error");
        }
    }

    /**
     * Check if the User has permission to do something
     * @param user
     * @param permission
     * @return
     */
    public ObjectResponse<Boolean> hasPermission(User user, String permission) {
        if(user.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.UNAUTHORIZED, String.format("User does not have permission to %s", permission), false);
        }

        if(permission.isEmpty()) {
            return new ObjectResponse<>(HttpCode.UNAUTHORIZED, String.format("User does not have permission to %s", permission), false);
        }

        Permission p = Iterables.tryFind(user.getRole().getPermissions(), input -> input.getName().contains(permission)).orNull();

        if(p == null) {
            return new ObjectResponse<>(HttpCode.UNAUTHORIZED, String.format("User does not have permission to %s", permission), false);
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("User has permission to %s", permission), true);
    }

    /**
     * Check if the User is an administrator
     * @param user
     * @return
     */
    public ObjectResponse<Boolean> isAdmin(User user) {
        if(user.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.UNAUTHORIZED, "User is not Admin");
        }

        Role r = user.getRole();

        if(r.getName().contains("admin")) {
            return new ObjectResponse<>(HttpCode.OK, String.format("User %s is admin", user.getEmail()), true);
        }

        return new ObjectResponse<>(HttpCode.UNAUTHORIZED, String.format("User %s does not have admin privileges", user.getEmail()), false);
    }
}
