package com.teamterry.administrationsystem.services;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.InvoiceItem;
import com.teamterry.administrationsystem.repositories.interfaces.InvoiceItemRepository;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class InvoiceItemService {

    @Inject
    @JPA
    private InvoiceItemRepository invoiceItemRepository;

    public ObjectResponse<List<InvoiceItem>> getAll() {
        List<InvoiceItem> invoiceItems = invoiceItemRepository.findAll();

        if(invoiceItems == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "No existing invoice items");
        }

        if(invoiceItems.size() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "No existing invoice items");
        }

        return new ObjectResponse<>(HttpCode.OK, "All invoice items", invoiceItems);
    }

    public ObjectResponse<List<InvoiceItem>> getAllFromInvoice(Invoice invoice){
        if(invoice == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Invoice is null");
        }

        if(invoice.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Invoice with id %s is invalid", invoice.getId()));
        }

        List<InvoiceItem> invoiceItems = invoiceItemRepository.getAllFromInvoice(invoice);

        if(invoiceItems == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("InvoiceItems not found for invoice with id %s", invoice.getId()));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("InvoiceItems for Invoice with id %s", invoice.getId()), invoiceItems);
    }

    public ObjectResponse<InvoiceItem> getFromID(int id) {
        if(id <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("Id %s is invalid", id));
        }

        InvoiceItem invoiceItem = invoiceItemRepository.findOne(id);

        if(invoiceItem == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("InvoiceItem with id %s not found", id));
            }

        return new ObjectResponse<>(HttpCode.OK, String.format("InvoiceItem with id %s", id), invoiceItem);
    }

    public ObjectResponse<InvoiceItem> getFromName(String name) {
        if(name == null) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Name %s is empty");
        }

        if(name.length() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "Name %s is empty");
        }

        InvoiceItem invoiceItem = invoiceItemRepository.getfromName(name);

        if(invoiceItem == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("InvoiceItem with name %s not found", name));
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("InvoiceItem with name %s", name), invoiceItem);
    }

    public ObjectResponse<InvoiceItem> addInvoiceItem(InvoiceItem invoiceItem) {
        if(invoiceItem == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "InvoiceItem is null");
        }

        if(invoiceItem.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("InvoiceItem with id %s is invalid", invoiceItem.getId()));
        }

        if(invoiceItem.getName().length() <= 0 || invoiceItem.getName().length() > 25){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("InvoiceItem with name %s is invalid", invoiceItem.getName()));
        }

        InvoiceItem result = invoiceItemRepository.save(invoiceItem);

        if(result == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "Failed to create invoiceItem");
        }

        return new ObjectResponse<>(HttpCode.OK, "New invoiceItem created", result);
    }

    public ObjectResponse<InvoiceItem> editInvoiceItem(InvoiceItem invoiceItem) {
        if(invoiceItem == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "InvoiceItem is null");
        }

        if(invoiceItem.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("InvoiceItem with id %s is invalid", invoiceItem.getId()));
        }

        if(invoiceItem.getName().length() <= 0 || invoiceItem.getName().length() > 25){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("InvoiceItem with name %s is invalid", invoiceItem.getName()));
        }

        InvoiceItem result = invoiceItemRepository.save(invoiceItem);

        if(result == null) {
            return new ObjectResponse<>(HttpCode.NOT_FOUND, "Failed to edit InvoiceItem");
        }

        return new ObjectResponse<>(HttpCode.OK, String.format("Invoice with id %s edited", invoiceItem.getId()), result);
    }

    public ObjectResponse<Boolean> removeInvoiceItem(InvoiceItem invoiceItem) {
        if(invoiceItem == null){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, "InvoiceItem is null");
        }

        if(invoiceItem.getId() <= 0) {
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("InvoiceItem with id %s is invalid", invoiceItem.getId()));
        }

        if(invoiceItem.getName().length() <= 0 || invoiceItem.getName().length() > 25){
            return new ObjectResponse<>(HttpCode.NOT_ACCEPTABLE, String.format("InvoiceItem with name %s is invalid", invoiceItem.getName()));
        }

        invoiceItemRepository.delete(invoiceItem);

        return new ObjectResponse<>(HttpCode.OK, String.format("InvoiceItem with id %s removed", invoiceItem.getId()), true);
    }
}
