package com.teamterry.administrationsystem.utils;

import com.google.common.flogger.FluentLogger;

public class Logger {
    private static final FluentLogger logger = FluentLogger.forEnclosingClass();

    public static FluentLogger getLogger() {
        return logger;
    }
}
