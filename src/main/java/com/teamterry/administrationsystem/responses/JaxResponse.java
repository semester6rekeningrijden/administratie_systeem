package com.teamterry.administrationsystem.responses;

import javax.ws.rs.core.Response;

public class JaxResponse {
    public static Response checkObjectResponse(ObjectResponse response) {
        return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
    }
}
