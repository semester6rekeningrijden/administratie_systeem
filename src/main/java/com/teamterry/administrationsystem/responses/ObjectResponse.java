package com.teamterry.administrationsystem.responses;

public class ObjectResponse<T> {
    private HttpCode code;
    private String message = null;
    private T object;

    public ObjectResponse(HttpCode code, String message, T object) {
        this.code = code;
        this.message = message;
        this.object = object;
    }

    public ObjectResponse(HttpCode code, String message) {
        this.code = code;
        this.message = message;
    }

    public HttpCode getCode() {
        return code;
    }

    public void setCode(HttpCode code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
