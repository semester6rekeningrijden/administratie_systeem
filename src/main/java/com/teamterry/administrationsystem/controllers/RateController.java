package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.AuthenticationProvider;
import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.domain.Rate;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.rates.CreateRateRequestObject;
import com.teamterry.administrationsystem.dtos.rates.EditRateRequestObject;
import com.teamterry.administrationsystem.dtos.rates.RateDto;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.RateService;
import com.teamterry.administrationsystem.utils.Logger;
import org.modelmapper.ModelMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
@Path("/rates")
public class RateController {
    @Inject
    private RateService rateService;

    @Inject
    private AuthenticationProvider authenticationProvider;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@HeaderParam("Authorization") String authentication) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<Rate>> response = rateService.all();

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RateDto[] rateDto = mapper.map(response.getObject(), RateDto[].class);
            return Response.status(response.getCode().getValue()).entity(rateDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getById(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Rate> response = rateService.findById(id);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RateDto rateDto = mapper.map(response.getObject(), RateDto.class);

            return Response.status(response.getCode().getValue()).entity(rateDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(@HeaderParam("Authorization") String authentication, CreateRateRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.CREATE_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            Rate rate = new Rate();
            rate.setPrice(request.getPrice());
            rate.setStartDate(request.getStartDate());
            rate.setEndDate(request.getEndDate());

            if(request.getVehicleType() != null) {
                rate.setVehicleType(request.getVehicleType());
            }

            if(request.getRoadType() != null) {
                rate.setRoadType(request.getRoadType());
            }

            if(request.getRecurring() != null) {
                rate.setRecurring(request.getRecurring());
            }

            ObjectResponse<Rate> response = rateService.create(rate);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RateDto rateDto = mapper.map(response.getObject(), RateDto.class);

            return Response.status(response.getCode().getValue()).entity(rateDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response update(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, EditRateRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.UPDATE_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            if(id != request.getId()) {
                return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.NOT_ACCEPTABLE, "Given ID's do not match"));
            }

            Rate rate = new Rate();
            rate.setId(request.getId());
            rate.setPrice(request.getId());
            rate.setPrice(request.getPrice());
            rate.setStartDate(request.getStartDate());
            rate.setEndDate(request.getEndDate());

            if(request.getVehicleType() != null) {
                rate.setVehicleType(request.getVehicleType());
            }

            if(request.getRoadType() != null) {
                rate.setRoadType(request.getRoadType());
            }

            if(request.getRecurring() != null) {
                rate.setRecurring(request.getRecurring());
            }

            ObjectResponse<Rate> response = rateService.update(rate);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RateDto rateDto = mapper.map(response.getObject(), RateDto.class);

            return Response.status(response.getCode().getValue()).entity(rateDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response delete(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.DELETE_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Rate> response = rateService.findById(id);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ObjectResponse<Rate> result = rateService.delete(response.getObject().getId());

            if(result.getObject() == null) {
                return JaxResponse.checkObjectResponse(result);
            }

            ModelMapper mapper = new ModelMapper();
            RateDto rateDto = mapper.map(response.getObject(), RateDto.class);

            return Response.status(result.getCode().getValue()).entity(rateDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }
}
