package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.AuthenticationProvider;
import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.roles.AttachRoleRequestObject;
import com.teamterry.administrationsystem.dtos.roles.CreateRoleRequestObject;
import com.teamterry.administrationsystem.dtos.roles.EditRoleRequestObject;
import com.teamterry.administrationsystem.dtos.roles.RoleDto;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.PermissionService;
import com.teamterry.administrationsystem.services.RoleService;
import com.teamterry.administrationsystem.utils.Logger;
import org.modelmapper.ModelMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
@Path("/roles")
public class RoleController extends Application {

    @Inject
    private RoleService roleService;

    @Inject
    private PermissionService permissionService;

    @Inject
    private AuthenticationProvider authenticationProvider;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@HeaderParam("Authorization") String authentication) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_ROLES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<Role>> response = roleService.all();

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto[] roleDto = mapper.map(response.getObject(), RoleDto[].class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(@HeaderParam("Authorization") String authentication, CreateRoleRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.CREATE_ROLES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            Role object = new Role(request.getName());

            ObjectResponse<Role> response = roleService.create(object);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto roleDto = mapper.map(response.getObject(), RoleDto.class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getById(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_ROLES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Role> response = roleService.findById(id);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto roleDto = mapper.map(response.getObject(), RoleDto.class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/name/{name}")
    public Response getByName(@HeaderParam("Authorization") String authentication, @PathParam("name") String name) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_ROLES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Role> response = roleService.findByName(name);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto roleDto = mapper.map(response.getObject(), RoleDto.class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response update(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, EditRoleRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.UPDATE_ROLES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            Role role = new Role(request.getName());
            role.setId(id);
            ObjectResponse<Role> response = roleService.update(role);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto roleDto = mapper.map(response.getObject(), RoleDto.class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response delete(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.DELETE_ROLES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Role> response = roleService.findById(id);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ObjectResponse<Role> result = roleService.delete(response.getObject());

            if(result.getObject() == null) {
                return JaxResponse.checkObjectResponse(result);
            }

            return Response.status(result.getCode().getValue()).entity(result.getMessage()).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/attach")
    public Response attachPermission(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, AttachRoleRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.ATTACH_PERMISSION_TO_ROLE.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            if(id != request.getRoleId()) {
                return Response.status(Response.Status.FORBIDDEN).entity("URL id is not the same as the body").build();
            }

            ObjectResponse<Role> getRoleByIdResponse = roleService.findById(request.getRoleId());

            if(getRoleByIdResponse.getObject() == null) {
                return Response.status(getRoleByIdResponse.getCode().getValue()).entity(getRoleByIdResponse.getMessage()).build();
            }

            ObjectResponse<Permission> getPermissionByIdResponse = permissionService.findById(request.getPermissionId());

            if(getPermissionByIdResponse.getObject() == null) {
                return Response.status(getPermissionByIdResponse.getCode().getValue()).entity(getPermissionByIdResponse.getMessage()).build();
            }

            ObjectResponse<Role> response = roleService.attachPermission(getRoleByIdResponse.getObject(), getPermissionByIdResponse.getObject());

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto roleDto = mapper.map(response.getObject(), RoleDto.class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/detach")
    public Response detachPermission(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, AttachRoleRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.DETACH_PERMISSION_FROM_ROLE.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            if(id != request.getRoleId()) {
                return Response.status(Response.Status.FORBIDDEN).entity("URL id is not the same as the body").build();
            }

            ObjectResponse<Role> getRoleByIdResponse = roleService.findById(request.getRoleId());

            if(getRoleByIdResponse.getObject() == null) {
                return Response.status(getRoleByIdResponse.getCode().getValue()).entity(getRoleByIdResponse.getMessage()).build();
            }

            ObjectResponse<Permission> getPermissionByIdResponse = permissionService.findById(request.getPermissionId());

            if(getPermissionByIdResponse.getObject() == null) {
                return Response.status(getPermissionByIdResponse.getCode().getValue()).entity(getPermissionByIdResponse.getMessage()).build();
            }

            ObjectResponse<Role> response = roleService.detachPermission(getRoleByIdResponse.getObject(), getPermissionByIdResponse.getObject());

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            RoleDto roleDto = mapper.map(response.getObject(), RoleDto.class);

            return Response.status(response.getCode().getValue()).entity(roleDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
