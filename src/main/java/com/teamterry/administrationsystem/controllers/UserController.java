package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.AuthenticationProvider;
import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.users.ChangeRoleRequestObject;
import com.teamterry.administrationsystem.dtos.users.EditUserRequestObject;
import com.teamterry.administrationsystem.dtos.users.UserDto;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.RoleService;
import com.teamterry.administrationsystem.services.UserService;
import com.teamterry.administrationsystem.utils.Logger;
import org.modelmapper.ModelMapper;

import javax.annotation.security.DeclareRoles;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.security.enterprise.authentication.mechanism.http.BasicAuthenticationMechanismDefinition;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
@Path("/users")
public class UserController extends Application {

    @Inject
    private UserService userService;

    @Inject
    private RoleService roleService;

    @Inject
    private AuthenticationProvider authenticationProvider;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@HeaderParam("Authorization") String authentication) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_USERS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<User>> response = userService.all();

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto[] userDto = mapper.map(response.getObject(), UserDto[].class);

            return Response.status(response.getCode().getValue()).entity(userDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getById(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_USERS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<User> response = userService.getById(id);

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto userDto = mapper.map(response.getObject(), UserDto.class);

            return Response.status(response.getCode().getValue()).entity(userDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/email/{email}")
    public Response getByEmail(@HeaderParam("Authorization") String authentication, @PathParam("email") String email) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_USERS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<User> response = userService.getByEmail(email);

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto userDto = mapper.map(response.getObject(), UserDto.class);

            return Response.status(response.getCode().getValue()).entity(userDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/name/{firstname}%{lastname}")
    public Response getByFirstLastName(@HeaderParam("Authorization") String authentication, @PathParam("firstname") String firstname, @PathParam("lastname") String lastname) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_USERS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<User>> response = userService.getByName(firstname, lastname);

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto userDto = mapper.map(response.getObject(), UserDto.class);

            return Response.status(response.getCode().getValue()).entity(userDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response update(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, EditUserRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.UPDATE_USERS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<User> response = userService.getById(id);

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            response.getObject().setFirstName(request.getFirstName() != null && !request.getFirstName().replaceAll(" ", "").isEmpty() ? request.getFirstName() : response.getObject().getFirstName());
            response.getObject().setLastName(request.getLastName() != null && !request.getLastName().replaceAll(" ", "").isEmpty() ? request.getLastName() : response.getObject().getLastName());
            response.getObject().setEmail(request.getEmail() != null && !request.getEmail().replaceAll(" ", "").isEmpty() ? request.getEmail() : response.getObject().getEmail());
            response.getObject().setStreet(request.getStreet() != null && !request.getStreet().replaceAll(" ", "").isEmpty() ? request.getStreet() : response.getObject().getStreet());
            response.getObject().setCity(request.getCity() != null && !request.getCity().replaceAll(" ", "").isEmpty() ? request.getCity() : response.getObject().getCity());
            response.getObject().setCountry(request.getCountry() != null && !request.getCountry().replaceAll(" ", "").isEmpty() ? request.getCountry() : response.getObject().getCountry());
            response.getObject().setStreet(request.getStreet() != null && !request.getStreet().replaceAll(" ", "").isEmpty() ? request.getStreet() : response.getObject().getStreet());

            ObjectResponse<User> result = userService.update(response.getObject());

            if(result.getObject() == null) {
                return Response.status(result.getCode().getValue()).entity(result.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto userDto = mapper.map(result.getObject(), UserDto.class);

            return Response.status(result.getCode().getValue()).entity(userDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response delete(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.DELETE_USERS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<User> response = userService.getById(id);

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ObjectResponse<User> result = userService.delete(response.getObject());

            if(result.getObject() == null) {
                return Response.status(result.getCode().getValue()).entity(result.getMessage()).build();
            }

            return Response.status(result.getCode().getValue()).entity(result.getMessage()).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}/role")
    public Response changeRole(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, ChangeRoleRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.CHANGE_ROLE_OF_USER.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            if(id != request.getUserId()) {
                return Response.status(Response.Status.FORBIDDEN).entity("URL id is not the same as the body").build();
            }

            ObjectResponse<User> getUserByIdResponse = userService.getById(request.getUserId());

            if(getUserByIdResponse.getObject() == null) {
                return Response.status(getUserByIdResponse.getCode().getValue()).entity(getUserByIdResponse.getMessage()).build();
            }

            ObjectResponse<Role> getRoleByIdResponse = roleService.findById(request.getRoleId());

            if(getRoleByIdResponse.getObject() == null) {
                return Response.status(getRoleByIdResponse.getCode().getValue()).entity(getRoleByIdResponse.getMessage()).build();
            }

            ObjectResponse<User> response = userService.changeRole(getUserByIdResponse.getObject(), getRoleByIdResponse.getObject());

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto userDto = mapper.map(response.getObject(), UserDto.class);

            return Response.status(response.getCode().getValue()).entity(userDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
