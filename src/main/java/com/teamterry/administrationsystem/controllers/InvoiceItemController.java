package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.AuthenticationProvider;
import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.domain.InvoiceItem;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.invoices.InvoiceItemDTO;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.InvoiceItemService;
import org.modelmapper.ModelMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
@Path("invoiceItems")
public class InvoiceItemController extends Application {
    @Inject
    private InvoiceItemService invoiceItemService;

    @Inject
    private AuthenticationProvider authenticationProvider;

    @POST
    @Produces("application/json")
    public Response addInvoiceItem(@HeaderParam("Authorization") String authentication, InvoiceItemDTO invoiceItemDTO){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceItem invoiceItem = mapper.map(invoiceItemDTO, InvoiceItem.class);
            ObjectResponse<InvoiceItem> response = invoiceItemService.addInvoiceItem(invoiceItem);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            InvoiceItemDTO[] invoiceItemDto = mapper.map(response.getObject(), InvoiceItemDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceItemDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response edit(@HeaderParam("Authorization") String authentication, InvoiceItem invoiceItem){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<InvoiceItem> response = invoiceItemService.editInvoiceItem(invoiceItem);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceItemDTO[] invoiceItemDto = mapper.map(response.getObject(), InvoiceItemDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceItemDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Produces("text/plain")
    public Response remove(@HeaderParam("Authorization") String authentication, InvoiceItem invoiceItem){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Boolean> response = invoiceItemService.removeInvoiceItem(invoiceItem);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceItemDTO[] invoiceItemDto = mapper.map(response.getObject(), InvoiceItemDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceItemDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response getFromID(@HeaderParam("Authorization") String authentication, @PathParam("id") int id){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<InvoiceItem> response = invoiceItemService.getFromID(id);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceItemDTO[] invoiceItemDto = mapper.map(response.getObject(), InvoiceItemDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceItemDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/search/{name}")
    @Produces("application/json")
    public Response getFromName(@HeaderParam("Authorization") String authentication, @PathParam("name") String name){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<InvoiceItem> response = invoiceItemService.getFromName(name);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceItemDTO[] invoiceItemDto = mapper.map(response.getObject(), InvoiceItemDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceItemDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Produces("application/json")
    public Response getAll(@HeaderParam("Authorization") String authentication){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<InvoiceItem>> response = invoiceItemService.getAll();
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceItemDTO[] invoiceItemDto = mapper.map(response.getObject(), InvoiceItemDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceItemDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
