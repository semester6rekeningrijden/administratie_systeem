package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.AuthenticationProvider;
import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.authentication.TokenProvider;
import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.permissions.CreatePermissionRequestObject;
import com.teamterry.administrationsystem.dtos.permissions.EditPermissionRequestObject;
import com.teamterry.administrationsystem.dtos.permissions.PermissionDto;
import com.teamterry.administrationsystem.dtos.users.UserDto;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.PermissionService;
import com.teamterry.administrationsystem.services.UserService;
import com.teamterry.administrationsystem.utils.Logger;
import org.modelmapper.ModelMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Stateless
@Path("/permissions")
public class PermissionController extends Application {

    @Inject
    private PermissionService permissionService;

    @Inject
    private AuthenticationProvider authenticationProvider;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@HeaderParam("Authorization") String authentication) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_PERMISSIONS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<Permission>> response = permissionService.all();

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            PermissionDto[] permissionDto = mapper.map(response.getObject(), PermissionDto[].class);
            Logger.getLogger().atSevere().log("GET ALL PERMISSIONS");
            return Response.status(response.getCode().getValue()).entity(permissionDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response getById(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_PERMISSIONS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Permission> response = permissionService.findById(id);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            PermissionDto permissionDto = mapper.map(response.getObject(), PermissionDto.class);

            return Response.status(response.getCode().getValue()).entity(permissionDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/name/{name}")
    public Response getByName(@HeaderParam("Authorization") String authentication, @PathParam("name") String name) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_PERMISSIONS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Permission> response = permissionService.findByName(name);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            PermissionDto permissionDto = mapper.map(response.getObject(), PermissionDto.class);

            return Response.status(response.getCode().getValue()).entity(permissionDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(@HeaderParam("Authorization") String authentication, CreatePermissionRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.CREATE_PERMISSIONS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            Permission permission = new Permission(request.getName());

            ObjectResponse<Permission> response = permissionService.create(permission);

            if(response.getObject() == null) {
               return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            PermissionDto permissionDto = mapper.map(response.getObject(), PermissionDto.class);

            return Response.status(response.getCode().getValue()).entity(permissionDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response update(@HeaderParam("Authorization") String authentication, @PathParam("id") int id, EditPermissionRequestObject request) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.UPDATE_PERMISSIONS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            Permission permission = new Permission(request.getName());
            permission.setId(id);
            ObjectResponse<Permission> response = permissionService.update(permission);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ModelMapper mapper = new ModelMapper();
            PermissionDto permissionDto = mapper.map(response.getObject(), PermissionDto.class);

            return Response.status(response.getCode().getValue()).entity(permissionDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response delete(@HeaderParam("Authorization") String authentication, @PathParam("id") int id) {
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.DELETE_PERMISSIONS.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Permission> response = permissionService.findById(id);

            if(response.getObject() == null) {
                return JaxResponse.checkObjectResponse(response);
            }

            ObjectResponse<Permission> result = permissionService.delete(response.getObject());

            if(result.getObject() == null) {
                return JaxResponse.checkObjectResponse(result);
            }

            ModelMapper mapper = new ModelMapper();
            PermissionDto permissionDto = mapper.map(response.getObject(), PermissionDto.class);

            return Response.status(result.getCode().getValue()).entity(permissionDto).build();
        }catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return JaxResponse.checkObjectResponse(new ObjectResponse(HttpCode.INTERNAL_SERVER_ERROR, e.getMessage()));
        }
    }
}
