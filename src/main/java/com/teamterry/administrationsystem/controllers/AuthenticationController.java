package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.TokenProvider;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.users.CreateUserRequestObject;
import com.teamterry.administrationsystem.dtos.users.LoginRequestObject;
import com.teamterry.administrationsystem.dtos.users.TokenDto;
import com.teamterry.administrationsystem.dtos.users.UserDto;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.UserService;
import com.teamterry.administrationsystem.utils.Logger;
import org.modelmapper.ModelMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Stateless
@Path("/auth")
public class AuthenticationController {

    @Inject
    private UserService userService;

    @Context
    private UriInfo uriInfo;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response login(LoginRequestObject loginViewModel) {
        try {
            ObjectResponse<User> response = userService.login(loginViewModel.getEmail(), loginViewModel.getPassword());

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ObjectResponse<String> tokenGeneration = TokenProvider.generate(Integer.toString(response.getObject().getId()), TokenProvider.ISSUER, response.getObject().getEmail(), TokenProvider.TIME_TO_LIVE);

            if(!tokenGeneration.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(tokenGeneration);
            }

            TokenDto token = new TokenDto();
            token.setToken(tokenGeneration.getObject());
            token.setUserId(response.getObject().getId());

            return Response.status(response.getCode().getValue()).entity(token).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/register")
    public Response create(CreateUserRequestObject request) {
        try {
            User user = new User();
            user.setEmail(request.getEmail());
            user.setPassword(request.getPassword());
            user.setCity(request.getCity());
            user.setCountry(request.getCountry());
            user.setStreet(request.getStreet());
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setZipcode(request.getZipcode());

            ObjectResponse<User> response = userService.create(user);

            if(response.getObject() == null) {
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            UserDto userDto = mapper.map(response.getObject(), UserDto.class);

            return Response.status(response.getCode().getValue()).entity(userDto).build();
        } catch (Exception e) {
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
