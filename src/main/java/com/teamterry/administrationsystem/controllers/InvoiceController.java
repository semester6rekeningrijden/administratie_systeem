package com.teamterry.administrationsystem.controllers;

import com.teamterry.administrationsystem.authentication.AuthenticationProvider;
import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.dtos.invoices.InvoiceDTO;
import com.teamterry.administrationsystem.dtos.invoices.StoreInvoiceRequest;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.JaxResponse;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.InvoiceService;
import org.modelmapper.ModelMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.List;

@Stateless
@Path("invoices")
public class InvoiceController extends Application {
    @Inject
    private InvoiceService invoiceService;

    @Inject
    private AuthenticationProvider authenticationProvider;

    @POST
    @Produces("application/json")
    public Response addInvoice(@HeaderParam("Authorization") String authentication, StoreInvoiceRequest invoice){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            Invoice inv = new Invoice();
            inv.setInvoiceNumber(invoice.getInvoiceNumber());
            inv.setInvoiceStatus(invoice.getInvoiceStatus());
            inv.setVehicleTrackerId(invoice.getVehicleTrackerId());
            inv.setDate(LocalDateTime.now());

            ObjectResponse<Invoice> response = invoiceService.addInvoice(inv);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceDTO[] invoiceDto = mapper.map(response.getObject(), InvoiceDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @PUT
    @Produces("application/json")
    public Response edit(@HeaderParam("Authorization") String authentication, Invoice invoice){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Invoice> response = invoiceService.editInvoice(invoice);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceDTO[] invoiceDto = mapper.map(response.getObject(), InvoiceDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Produces("text/plain")
    public Response remove(@HeaderParam("Authorization") String authentication, Invoice invoice){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Boolean> response = invoiceService.removeInvoice(invoice);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceDTO[] invoiceDto = mapper.map(response.getObject(), InvoiceDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response getFromID(@HeaderParam("Authorization") String authentication, @PathParam("id") int id){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<Invoice> response = invoiceService.getFromID(id);
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceDTO[] invoiceDto = mapper.map(response.getObject(), InvoiceDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

    @GET
    @Produces("application/json")
    public Response getAll(@HeaderParam("Authorization") String authentication){
        try {
            ObjectResponse<User> loggedIn = authenticationProvider.authenticationWithPermission(authentication, PermissionEnum.READ_RATES.getValue());

            if(!loggedIn.getCode().equals(HttpCode.OK)) {
                return JaxResponse.checkObjectResponse(loggedIn);
            }

            ObjectResponse<List<Invoice>> response = invoiceService.getAll();
            if(response.getObject() == null){
                return Response.status(response.getCode().getValue()).entity(response.getMessage()).build();
            }

            ModelMapper mapper = new ModelMapper();
            InvoiceDTO[] invoiceDto = mapper.map(response.getObject(), InvoiceDTO[].class);

            return Response.status(response.getCode().getValue()).entity(invoiceDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
