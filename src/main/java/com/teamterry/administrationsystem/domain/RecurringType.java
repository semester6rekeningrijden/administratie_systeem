package com.teamterry.administrationsystem.domain;

public enum RecurringType {
    NONE,
    DAILY,
    WORKDAYS,
    WEEKENDS,
    WEEKLY,
    MONTHLY,
    YEARLY,
}
