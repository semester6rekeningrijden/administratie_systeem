package com.teamterry.administrationsystem.domain;


import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.checkerframework.checker.nullness.qual.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r"),
        @NamedQuery(name = "Role.findByName", query = "SELECT r FROM Role r WHERE r.name = :name")
})
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH}, fetch = FetchType.LAZY)
    List<Permission> permissions;

    public Role() {
        this.permissions = new ArrayList<>();
    }

    public Role(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public void attachPermission(Permission permission) {
        this.permissions.add(permission);
        permission.attachRole(this);
    }

    public void detachPermission(Permission permission) {
        this.permissions.remove(permission);
        permission.detachRole(this);
    }

    public boolean hasPermission(String permission) {
        Permission p = Iterables.tryFind(this.permissions, new Predicate<Permission>() {
            @Override
            public boolean apply(@Nullable Permission input) {
                return input.getName().equals(permission);
            }
        }).orNull();

        return p != null;
    }
}
