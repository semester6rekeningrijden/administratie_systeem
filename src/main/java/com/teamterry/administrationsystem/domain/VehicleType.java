package com.teamterry.administrationsystem.domain;

public enum VehicleType {
    CAR,
    BUS,
    TRUCK
}
