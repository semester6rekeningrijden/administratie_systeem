package com.teamterry.administrationsystem.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@NamedQueries({
        @NamedQuery(name = "Rate.findAll", query = "SELECT r FROM Rate r")
})
public class Rate implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private double price;
    @Column(nullable = false)
    private LocalDateTime startDate;
    @Column(nullable = false)
    private LocalDateTime endDate;

    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;

    @Enumerated(EnumType.STRING)
    private RoadType roadType;

    @Enumerated(EnumType.STRING)
    private RecurringType recurring;

    public Rate(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) { this.id = id;}

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public RoadType getRoadType() {
        return roadType;
    }

    public void setRoadType(RoadType roadType) {
        this.roadType = roadType;
    }

    public RecurringType getRecurring() {
        return recurring;
    }

    public void setRecurring(RecurringType recurring) {
        this.recurring = recurring;
    }
}
