package com.teamterry.administrationsystem.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Invoice.findAll", query = "SELECT i FROM Invoice i"),
        @NamedQuery(name = "Invoice.getAll", query = "select i from Invoice as i"),
        @NamedQuery(name = "Invoice.getAllFromUser", query = "select i from Invoice as i where i.user.id = :id")
})
public class Invoice implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private int invoiceNumber;

    @Column(nullable = false)
    private int vehicleTrackerId;

    @Enumerated(EnumType.STRING)
    private InvoiceStatus invoiceStatus;

    @Column
    private LocalDateTime date;

    @ManyToOne
    private User user;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "invoice", fetch = FetchType.EAGER)
    private List<InvoiceItem> items;

    public Invoice(){
        this.date = LocalDateTime.now();
    }

    public Invoice(int invoiceNumber, int vehicleTrackerId, InvoiceStatus invoiceStatus, LocalDateTime date, User user) {
        this.invoiceNumber = invoiceNumber;
        this.vehicleTrackerId = vehicleTrackerId;
        this.invoiceStatus = invoiceStatus;
        this.date = date;
        this.user = user;
        this.items = new ArrayList<>();
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(int invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public int getVehicleTrackerId() {
        return vehicleTrackerId;
    }

    public void setVehicleTrackerId(int vehicleTrackerId) {
        this.vehicleTrackerId = vehicleTrackerId;
    }

    public double getTotalPrice() {
        double totalPrice = 0;
        for (InvoiceItem ii: this.items) {
            totalPrice += ii.getPrice();
        }
        return totalPrice;
    }

    public InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<InvoiceItem> getItems() {
        return items;
    }

    public void setItems(List<InvoiceItem> items) {
        this.items = items;
    }


}
