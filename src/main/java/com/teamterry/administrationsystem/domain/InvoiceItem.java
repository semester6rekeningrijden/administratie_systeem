package com.teamterry.administrationsystem.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = "InvoiceItem.getByName", query = "select ii from InvoiceItem as ii where ii.name = :name"),
        @NamedQuery(name = "InvoiceItem.getAllFromInvoice", query = "select ii from InvoiceItem as ii where ii.invoice.id = :id")
})
public class InvoiceItem implements Serializable {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false, length = 25)
    private String name;
    @Column
    private String description;
    @Column
    private double price;

    @ManyToOne
    private Invoice invoice;

    public InvoiceItem(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
