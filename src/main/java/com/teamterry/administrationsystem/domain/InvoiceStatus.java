package com.teamterry.administrationsystem.domain;

public enum InvoiceStatus {
    OPEN,
    PAYED,
    CANCELED
}
