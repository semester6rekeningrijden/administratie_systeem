package com.teamterry.administrationsystem.domain;

public enum RoadType {
    A_ROAD,
    N_ROAD,
    RING_ROAD,
    IN_CITY_ROAD_FAST,
    IN_CITY_ROAD_SLOW,
    OUT_OF_CITY_ROAD,
    DIRT_ROAD,
    UNKNOWN
}