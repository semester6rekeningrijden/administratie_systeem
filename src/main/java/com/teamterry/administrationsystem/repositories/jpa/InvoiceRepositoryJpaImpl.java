package com.teamterry.administrationsystem.repositories.jpa;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.repositories.interfaces.InvoiceRepository;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@JPA
@Stateless
public class InvoiceRepositoryJpaImpl extends AbstractCrudRepositoryJpaImpl<Invoice, Integer> implements InvoiceRepository {
    @PersistenceContext(unitName = "payaraHibernate")
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected Class<Invoice> getDomainClass() {
        return Invoice.class;
    }

    @Override
    public List<Invoice> getAllFromUser(User u){
        return em.createNamedQuery("Invoice.getAllFromUser", Invoice.class).setParameter("id", u.getId()).getResultList();
    }
}
