package com.teamterry.administrationsystem.repositories.jpa;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.RoleRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@JPA
@Stateless
public class RoleRepositoryJPAImpl extends AbstractCrudRepositoryJpaImpl<Role, Integer> implements RoleRepository {
    @PersistenceContext(unitName = "payaraHibernate")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected Class<Role> getDomainClass() {
        return Role.class;
    }


    @Override
    public Role findByName(String name) {
        try {
            return em.createNamedQuery("Role.findByName", Role.class).setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Role attachPermission(Role role, Permission permission) {
        try {
            role.attachPermission(permission);
            return em.merge(role);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Role detachPermission(Role role, Permission permission) {
        try {
            role.detachPermission(permission);
            return em.merge(role);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}