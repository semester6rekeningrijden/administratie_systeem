package com.teamterry.administrationsystem.repositories.jpa;

import com.teamterry.administrationsystem.domain.Rate;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.RateRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@JPA
@Stateless
public class RateRepositoryJpaImpl extends AbstractCrudRepositoryJpaImpl<Rate, Integer> implements RateRepository {
    @PersistenceContext(unitName = "payaraHibernate")
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected Class<Rate> getDomainClass() {
        return Rate.class;
    }
}
