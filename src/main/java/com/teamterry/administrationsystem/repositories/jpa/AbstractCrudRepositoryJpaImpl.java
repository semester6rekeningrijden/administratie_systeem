package com.teamterry.administrationsystem.repositories.jpa;


import com.teamterry.administrationsystem.repositories.interfaces.CrudRepository;
import com.teamterry.administrationsystem.utils.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public abstract class AbstractCrudRepositoryJpaImpl<T, Id extends Serializable> implements CrudRepository<T, Id> {

    abstract protected EntityManager getEntityManager();
    abstract protected Class<T> getDomainClass();

    @Override
    public T findOne(Id id) {
        return getEntityManager().find(getDomainClass(), id);
    }

    @Override
    public List<T> findAll() {
        try {
            EntityManager em = getEntityManager();
            if(em == null){
                return Collections.emptyList();
            }
            return em.createNamedQuery(getDomainClass().getSimpleName()+".findAll", getDomainClass()).getResultList();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    @Transactional(value = Transactional.TxType.REQUIRED)
    public T save(T entity) {
        try {
            if(getEntityManager().contains(entity)) {
                entity = getEntityManager().merge(entity);
            }

            getEntityManager().persist(entity);
            return entity;
        } catch(Exception e) {
            e.printStackTrace();
            Logger.getLogger().atInfo().withCause(e).log("Exception with message %s", e.getMessage());
            return null;
        }
    }

    @Override
    @Transactional(value = Transactional.TxType.REQUIRED)
    public List<T> save(Iterable<T> entity) {
        try {
            for (T t : entity) {
                if(!getEntityManager().contains(t)) {
                    t = getEntityManager().merge(t);
                }
                getEntityManager().persist(t);
            }
            return (List<T>) entity;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void delete(Id id) {
        try {
            T entity = findOne(id);
            if (entity != null) {
                delete(entity);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(T entity) {
        try {
            if (entity != null) {
                if(!getEntityManager().contains(entity)) {
                    entity = getEntityManager().merge(entity);
                }

                getEntityManager().remove(entity);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Iterable<? extends T> entities) {
        try {
            for (T entity : entities) {
                delete(entity);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
