package com.teamterry.administrationsystem.repositories.jpa;

import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.UserRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@JPA
@Stateless
public class UserRepositoryJPAImpl extends AbstractCrudRepositoryJpaImpl<User, Integer> implements UserRepository {
    @PersistenceContext(unitName = "payaraHibernate")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected Class<User> getDomainClass() {
        return User.class;
    }

    @Override
    public User findByEmail(String email) {
        try {
            return em.createNamedQuery("User.findByEmail", User.class).setParameter("email", email).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<User> findByName(String firstname, String lastname) {
        try {
            return em.createNamedQuery("User.findByName", User.class).setParameter("firstname", firstname).setParameter("lastname", lastname).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<User> findByZipcode(String zipcode) {
        try {
            return em.createNamedQuery("User.findByZipcode", User.class).setParameter("zipcode", zipcode).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User changeRole(User user, Role role) {
        try {
            user.setRole(role);
            return save(user);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User login(String email, String password) {
        try {
            List<User> users = em.createNamedQuery("User.login", User.class).setParameter("email", email).setParameter("password", password).getResultList();
            User found = null;

            if(!users.isEmpty()) {
                found = users.get(0);
            }

            return found;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
