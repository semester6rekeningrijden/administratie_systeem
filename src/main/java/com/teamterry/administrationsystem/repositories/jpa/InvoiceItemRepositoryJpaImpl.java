package com.teamterry.administrationsystem.repositories.jpa;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.InvoiceItem;
import com.teamterry.administrationsystem.repositories.interfaces.InvoiceItemRepository;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@JPA
@Stateless
public class InvoiceItemRepositoryJpaImpl extends AbstractCrudRepositoryJpaImpl<InvoiceItem, Integer> implements InvoiceItemRepository {
    @PersistenceContext(unitName = "payaraHibernate")
    EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected Class<InvoiceItem> getDomainClass() {
        return InvoiceItem.class;
    }

    @Override
    public List<InvoiceItem> getAllFromInvoice(Invoice invoice) {
        return em.createNamedQuery("InvoiceItem.getAllFromInvoice", InvoiceItem.class).setParameter("id", invoice.getId()).getResultList();
    }

    @Override
    public InvoiceItem getfromName(String name) {
        return em.createNamedQuery("InvoiceItem.getByName", InvoiceItem.class).setParameter("name", name).getSingleResult();
    }
}
