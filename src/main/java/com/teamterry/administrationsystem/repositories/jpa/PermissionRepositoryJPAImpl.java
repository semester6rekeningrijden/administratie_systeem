package com.teamterry.administrationsystem.repositories.jpa;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.repositories.interfaces.JPA;
import com.teamterry.administrationsystem.repositories.interfaces.PermissionRepository;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@JPA
@Stateless
public class PermissionRepositoryJPAImpl extends AbstractCrudRepositoryJpaImpl<Permission, Integer> implements PermissionRepository {
    @PersistenceContext(unitName = "payaraHibernate")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected Class<Permission> getDomainClass() {
        return Permission.class;
    }

    @Override
    public Permission findByName(String name) {
        try {
            return em.createNamedQuery("Permission.findByName", Permission.class).setParameter("name", name).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
