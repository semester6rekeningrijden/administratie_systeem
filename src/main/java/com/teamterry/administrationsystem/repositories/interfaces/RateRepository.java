package com.teamterry.administrationsystem.repositories.interfaces;

import com.teamterry.administrationsystem.domain.Rate;

public interface RateRepository extends CrudRepository<Rate, Integer> {
}
