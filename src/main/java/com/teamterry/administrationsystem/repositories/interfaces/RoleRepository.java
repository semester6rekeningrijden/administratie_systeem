package com.teamterry.administrationsystem.repositories.interfaces;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.repositories.interfaces.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByName(String name);
    Role attachPermission(Role role, Permission permission);
    Role detachPermission(Role role, Permission permission);
}
