package com.teamterry.administrationsystem.repositories.interfaces;

import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;

import java.util.List;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);
    List<User> findByName(String firstname, String lastname);
    List<User> findByZipcode(String zipcode);
    User changeRole(User user, Role role);
    User login(String email, String password);
}
