package com.teamterry.administrationsystem.repositories.interfaces;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.InvoiceItem;

import java.util.List;

public interface InvoiceItemRepository extends CrudRepository<InvoiceItem, Integer> {
    List<InvoiceItem> getAllFromInvoice(Invoice invoice);
    InvoiceItem getfromName(String name);
}
