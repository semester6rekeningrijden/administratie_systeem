package com.teamterry.administrationsystem.repositories.interfaces;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public interface CrudRepository<T, Id extends Serializable> {
    void delete(Id id);
    void delete(T entity);
    void delete(Iterable<? extends T> entities);
    T findOne(Id id);
    List<T> findAll();
    T save(T entity);

    @Transactional
    List<T> save(Iterable<T> entities);
}
