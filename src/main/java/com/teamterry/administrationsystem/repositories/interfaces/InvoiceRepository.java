package com.teamterry.administrationsystem.repositories.interfaces;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.User;

import java.util.List;

public interface InvoiceRepository extends CrudRepository<Invoice, Integer> {
    List<Invoice> getAllFromUser(User u);
}
