package com.teamterry.administrationsystem.repositories.interfaces;


import com.teamterry.administrationsystem.domain.Permission;

public interface PermissionRepository extends CrudRepository<Permission, Integer> {
    Permission findByName(String name);
}
