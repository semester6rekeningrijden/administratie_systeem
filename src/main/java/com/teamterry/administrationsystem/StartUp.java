package com.teamterry.administrationsystem;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.PermissionService;
import com.teamterry.administrationsystem.services.RoleService;
import com.teamterry.administrationsystem.services.UserService;
import com.teamterry.administrationsystem.utils.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;

@Singleton
@Startup
@ApplicationScoped
public class StartUp {
    @Inject
    private UserService userService;

    @Inject
    private RoleService roleService;

    @Inject
    private PermissionService permissionService;

    @PostConstruct
    public void initData() {
        try {
            setPermissions();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
        }
        setRoles();
        setUsers();
    }

    /**
     * Pre-populate permissions.
     */
    private void setPermissions() throws Exception {
        Set<String> permissions = new HashSet<>();
        permissions.add("permissions");
        permissions.add("roles");
        permissions.add("users");
        permissions.add("invoices");
        permissions.add("rates");

        /**
         * Default CRUD Functionality
         */
        permissions.forEach(p -> {
            permissionService.create(new Permission(String.format("create_%s", p)));
            permissionService.create(new Permission(String.format("read_%s", p)));
            permissionService.create(new Permission(String.format("update_%s", p)));
            permissionService.create(new Permission(String.format("delete_%s", p)));
        });

        // Be able to change the role of a user
        permissionService.create(new Permission("changerole_user"));
        permissionService.create(new Permission("attach_permission"));
        permissionService.create(new Permission("detach_permission"));
    }

    /**
     * Pre-populate roles
     */
    private void setRoles() {
        try {
            ObjectResponse<Role> created = roleService.create(new Role("admin"));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
        }

        try {
            ObjectResponse<Role> created = roleService.create(new Role("employee"));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
        }

        try {
            ObjectResponse<Role> created = roleService.create(new Role("citizen"));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
        }
    }

    /**
     * Pre-populate users
     */
    private void setUsers()  {
        try {
            // USER: Create admin user
            User user = new User();
            user.setPassword("password123");
            user.setEmail("admin@admin.com");
            user.setFirstName("Admin");
            user.setLastName("User");
            user.setStreet("Rachelsmolen 1");
            user.setZipcode("5611AM");
            user.setCountry("Netherlands");
            user.setCity("Eindhoven");

            ObjectResponse<User> created = userService.create(user);

            if(created.getObject() != null) {
                ObjectResponse<Role> role = roleService.findByName("admin");

                if(role.getObject() != null) {
                    userService.changeRole(created.getObject(), role.getObject());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger().atInfo().withCause(e).log("Exception with message: %s", e.getMessage());
        }
    }
}
