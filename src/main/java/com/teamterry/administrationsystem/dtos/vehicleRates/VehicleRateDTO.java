package com.teamterry.administrationsystem.dtos.vehicleRates;

import com.teamterry.administrationsystem.domain.RoadType;
import com.teamterry.administrationsystem.domain.VehicleType;

public class VehicleRateDTO {
    private double rate;
    private RoadType roadType;
    private VehicleType vehicleType;
    private String beginTime;
    private String endTime;

    public VehicleRateDTO() {
    }

    public VehicleRateDTO(double rate, RoadType roadType, VehicleType vehicleType, String beginTime, String endTime) {
        this.rate = rate;
        this.roadType = roadType;
        this.vehicleType = vehicleType;
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public RoadType getRoadType() {
        return roadType;
    }

    public void setRoadType(RoadType roadType) {
        this.roadType = roadType;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
