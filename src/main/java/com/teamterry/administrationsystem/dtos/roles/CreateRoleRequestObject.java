package com.teamterry.administrationsystem.dtos.roles;

public class CreateRoleRequestObject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
