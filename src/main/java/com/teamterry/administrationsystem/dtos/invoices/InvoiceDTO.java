package com.teamterry.administrationsystem.dtos.invoices;

import com.teamterry.administrationsystem.domain.InvoiceStatus;

import java.time.LocalDateTime;

public class InvoiceDTO {
    private Integer id;
    private Integer invoiceNumber;
    private Integer vehicleTrackerId;
    private Double totalPrice;
    private InvoiceStatus invoiceStatus;
    private LocalDateTime date;

    public InvoiceDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Integer getVehicleTrackerId() {
        return vehicleTrackerId;
    }

    public void setVehicleTrackerId(Integer vehicleTrackerId) {
        this.vehicleTrackerId = vehicleTrackerId;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
