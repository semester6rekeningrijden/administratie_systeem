package com.teamterry.administrationsystem.dtos.invoices;

import com.teamterry.administrationsystem.domain.InvoiceStatus;
import com.teamterry.administrationsystem.domain.User;

import java.time.LocalDateTime;

public class StoreInvoiceRequest {
    private Integer invoiceNumber;
    private Integer vehicleTrackerId;
    private InvoiceStatus invoiceStatus;

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Integer getVehicleTrackerId() {
        return vehicleTrackerId;
    }

    public void setVehicleTrackerId(Integer vehicleTrackerId) {
        this.vehicleTrackerId = vehicleTrackerId;
    }

    public InvoiceStatus getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }
}
