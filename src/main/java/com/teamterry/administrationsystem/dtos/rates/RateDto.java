package com.teamterry.administrationsystem.dtos.rates;

import com.teamterry.administrationsystem.domain.RecurringType;
import com.teamterry.administrationsystem.domain.RoadType;
import com.teamterry.administrationsystem.domain.VehicleType;

import java.time.LocalDateTime;

public class RateDto {
    private int id;
    private double price;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private VehicleType vehicleType;
    private RoadType roadType;
    private RecurringType recurring;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public RoadType getRoadType() {
        return roadType;
    }

    public void setRoadType(RoadType roadType) {
        this.roadType = roadType;
    }

    public RecurringType getRecurring() {
        return recurring;
    }

    public void setRecurring(RecurringType recurring) {
        this.recurring = recurring;
    }
}
