package com.teamterry.administrationsystem.services;

import com.teamterry.administrationsystem.DTOAbdo.JourneyDataDTO;
import com.teamterry.administrationsystem.domain.JourneyData;
import com.teamterry.administrationsystem.repositories.interfaces.JourneyDataRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class JourneyDataServiceTest {

    @InjectMocks
    JourneyDataService service;
    @Mock
    JourneyDataRepository repo;


    @Before
    public void init(){
        initMocks(this);
    }


    @Test
    public void createJourneyData() {
        // Arange
//        repo = Mockito.mock(JourneyDataRepository.class);
//        service = new JourneyDataService();
        JourneyDataDTO dto = new JourneyDataDTO();
        dto.setlatitude(2121);
        dto.setLongitude(2151);
        JourneyData data = new JourneyData(2151, 2121);
        when(repo.save(data)).thenReturn(data);



        // Act
        ObjectResponse<JourneyData> response = service.createJourneyData(dto);

        // Assert
        //verify(repo, times(1)).save(data);
        assertEquals(HttpCode.OK.getValue(), response.getCode());
        assertEquals(data.getLat(), response.getObject().getLat(),0);

    }
}