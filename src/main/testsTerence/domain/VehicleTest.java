package domain;

import com.teamterry.administrationsystem.domain.Vehicle;
import org.junit.Test;

import static org.junit.Assert.*;

public class VehicleTest {

    @Test
    public void getId_IdGiven_ShouldNotEqual0() {
        // Arrange
        Vehicle testVehicle = new Vehicle();

        // Act
        // Id is automatically generated

        // Assert
        assertEquals(0, testVehicle.getId());
    }

    @Test
    public void setId_IdIsOne_ShouldBeEqualToOne() {
        // Arrange
        Vehicle testVehicle = new Vehicle();

        // Act
        testVehicle.setId(1);

        // Assert
        assertEquals(1, testVehicle.getId());
    }

    @Test
    public void getLicensePlate() {

    }

    @Test
    public void setLicensePlate() {
        // TODO: check if valid license plate for country?
    }

    @Test
    public void getType() {
    }

    @Test
    public void setType() {
    }

    @Test
    public void getUser() {
    }

    @Test
    public void setUser() {
    }

    @Test
    public void getJourney() {
    }

    @Test
    public void setJourney() {
    }
}