package unit;

import com.teamterry.administrationsystem.domain.Invoice;
import com.teamterry.administrationsystem.domain.InvoiceItem;
import com.teamterry.administrationsystem.domain.InvoiceStatus;
import com.teamterry.administrationsystem.domain.User;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class InvoiceTest {

    public Invoice testInvoice(){
        User testUser = new User();
        testUser.setFirstName("Jelle");
        testUser.setLastName("Bouwmans");
        testUser.setEmail("jpa.bouwmans@fhict.nl");
        testUser.setPassword("geheim");
        testUser.setStreet("Mijn straat");
        testUser.setZipcode("5761EM");
        testUser.setCity("Bakel");
        testUser.setCountry("Netherlands");
        return new Invoice(1, 1, InvoiceStatus.OPEN, LocalDateTime.now(), testUser);
    }

    @Test
    public void getInvoiceNumber() {
        Invoice invoice = testInvoice();
        assertEquals(1, invoice.getInvoiceNumber());
    }

    @Test
    public void setInvoiceNumber() {
        Invoice invoice = testInvoice();
        invoice.setInvoiceNumber(2);
        assertNotEquals(1, invoice.getInvoiceNumber());
        assertEquals(2, invoice.getInvoiceNumber());
    }

    @Test
    public void getVehicleTrackerId() {
        Invoice invoice = testInvoice();
        assertEquals(1, invoice.getVehicleTrackerId());
    }

    @Test
    public void setVehicleTrackerId() {
        Invoice invoice = testInvoice();
        invoice.setVehicleTrackerId(2);
        assertNotEquals(1, invoice.getVehicleTrackerId());
        assertEquals(2, invoice.getVehicleTrackerId());
    }

    @Test
    public void getTotalPrice() {
        Invoice invoice = testInvoice();
        List<InvoiceItem> items = new ArrayList<>();
        InvoiceItem ii = new InvoiceItem();
        ii.setId(1);
        ii.setName("test");
        ii.setDescription("test item");
        ii.setPrice(15.50);
        items.add(ii);

        ii = new InvoiceItem();
        ii.setId(2);
        ii.setName("Test2");
        ii.setDescription("test item2");
        ii.setPrice(10);
        items.add(ii);

        ii = new InvoiceItem();
        ii.setId(3);
        ii.setName("Test3");
        ii.setDescription("test item3");
        ii.setPrice(9);
        items.add(ii);

        invoice.setItems(items);
        assertEquals(34.50, invoice.getTotalPrice(), 0.0001);
    }

    @Test
    public void getInvoiceStatus() {
        Invoice invoice = testInvoice();
        assertEquals(InvoiceStatus.OPEN, invoice.getInvoiceStatus());
    }

    @Test
    public void setInvoiceStatus() {
        Invoice invoice = testInvoice();
        invoice.setInvoiceStatus(InvoiceStatus.PAYED);
        assertNotEquals(InvoiceStatus.OPEN, invoice.getInvoiceStatus());
        assertEquals(InvoiceStatus.PAYED, invoice.getInvoiceStatus());
    }
}