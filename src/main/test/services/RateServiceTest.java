package services;

import com.teamterry.administrationsystem.domain.Rate;
import com.teamterry.administrationsystem.domain.RoadType;
import com.teamterry.administrationsystem.domain.VehicleType;
import com.teamterry.administrationsystem.repositories.interfaces.RateRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.RateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RateServiceTest {

    @InjectMocks
    private RateService rateService;

    @Mock
    private RateRepository rateRepository;

    @Test
    public void all_StatusCodeOk() {
        List<Rate> rates = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Rate rate = new Rate();
            rate.setPrice(15.54);
            rate.setStartDate(LocalDateTime.now());
            rate.setEndDate(LocalDateTime.now().plusWeeks(2));

            rates.add(rate);
        }
        when(rateRepository.findAll()).thenReturn(rates);

        ObjectResponse<List<Rate>> response = rateService.all();

        verify(rateRepository, atLeastOnce()).findAll();
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(rates.size(), response.getObject().size());
        assertEquals(rates, response.getObject());
    }

    @Test
    public void getById_ExistingId_StatusCodeOk() {
        // Arange
        int id = 6;
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().plusWeeks(2);
        Rate rate = new Rate();
        rate.setId(id);
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);
        when(rateRepository.findOne(id)).thenReturn(rate);

        // Act
        ObjectResponse<Rate> response = rateService.findById(id);

        // Assert
        verify(rateRepository, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(rate, response.getObject());
    }

    @Test
    public void getById_IdNull_StatusCodeNotAceptable() {
        // Arange
        int id = 0;

        // Act
        ObjectResponse<Rate> response = rateService.findById(id);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void getById_NotExistingId_StatusCodeNotFound() {
        // Arange
        int id = 6;
        when(rateRepository.findOne(id)).thenReturn(null);

        // Act
        ObjectResponse<Rate> response = rateService.findById(id);

        // Assert
        verify(rateRepository, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Create Permission Tests
     */
    @Test
    public void create_NewWithoutTypes_StatusCodeCreated() {
        // Arrange
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().plusWeeks(2);
        Rate rate = new Rate();
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);

        when(rateRepository.save(rate)).thenReturn(rate);

        // Act
        ObjectResponse<Rate> response = rateService.create(rate);

        // Assert
        verify(rateRepository, atLeastOnce()).save(rate);
        assertEquals(HttpCode.CREATED, response.getCode());
        assertEquals(rate.getPrice(), response.getObject().getPrice(), 2);
    }

    @Test
    public void create_NewWithTypes_StatusCodeCreated() {
        // Arrange
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().plusWeeks(2);
        VehicleType vehicleType = VehicleType.CAR;
        RoadType roadType = RoadType.IN_CITY_ROAD_FAST;
        Rate rate = new Rate();
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);
        rate.setVehicleType(vehicleType);
        rate.setRoadType(roadType);

        when(rateRepository.save(rate)).thenReturn(rate);

        // Act
        ObjectResponse<Rate> response = rateService.create(rate);

        // Assert
        verify(rateRepository, atLeastOnce()).save(rate);
        assertEquals(HttpCode.CREATED, response.getCode());
        assertEquals(rate.getPrice(), response.getObject().getPrice(), 2);
        assertEquals(rate.getRoadType(),response.getObject().getRoadType());
        assertEquals(rate.getVehicleType(), response.getObject().getVehicleType());
    }

    @Test
    public void create_EndDateBeforeStartDate_StatusCodeNotAcceptable() {
        // Arrange
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().minusWeeks(2);
        VehicleType vehicleType = VehicleType.CAR;
        RoadType roadType = RoadType.IN_CITY_ROAD_FAST;
        Rate rate = new Rate();
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);
        rate.setVehicleType(vehicleType);
        rate.setRoadType(roadType);


        // Act
        ObjectResponse<Rate> response = rateService.create(rate);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
    }

    @Test
    public void update_NewWithTypes_StatusCodeOk() {
        //Arange
        int id = 6;
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().plusWeeks(2);
        VehicleType vehicleType = VehicleType.CAR;
        RoadType roadType = RoadType.IN_CITY_ROAD_FAST;
        Rate rate = new Rate();
        rate.setId(id);
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);
        rate.setVehicleType(vehicleType);
        rate.setRoadType(roadType);

        when(rateRepository.findOne(id)).thenReturn(rate);
        when(rateRepository.save(rate)).thenReturn(rate);

        //Act
        ObjectResponse<Rate> response = rateService.update(rate);

        //Assert
        verify(rateRepository, atLeastOnce()).findOne(id);
        verify(rateRepository, atLeastOnce()).save(rate);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(rate.getPrice(), response.getObject().getPrice(), 2);
    }

    @Test
    public void update_NewWithoutTypes_StatusCodeOk() {
        //Arange
        int id = 6;
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().plusWeeks(2);
        Rate rate = new Rate();
        rate.setId(id);
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);

        when(rateRepository.findOne(id)).thenReturn(rate);
        when(rateRepository.save(rate)).thenReturn(rate);

        //Act
        ObjectResponse<Rate> response = rateService.update(rate);

        //Assert
        verify(rateRepository, atLeastOnce()).findOne(id);
        verify(rateRepository, atLeastOnce()).save(rate);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(rate.getPrice(), response.getObject().getPrice(), 2);
    }

    @Test
    public void update_Idnull_StatusCodeNotAcceptable() {
        //Arange
        int id = 0;

        Rate rate = new Rate();
        rate.setId(id);

        //Act
        ObjectResponse<Rate> response = rateService.update(rate);

        //Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
    }

    @Test
    public void update_RateNotExists_StatusCodeNotFound() {
        //Arange
        int id = 6;
        double price = 15.45;
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = LocalDateTime.now().plusWeeks(2);
        Rate rate = new Rate();
        rate.setId(id);
        rate.setPrice(price);
        rate.setStartDate(start);
        rate.setEndDate(end);

        when(rateRepository.findOne(id)).thenReturn(null);

        //Act
        ObjectResponse<Rate> response = rateService.update(rate);

        //Assert
        verify(rateRepository, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
    }
}
