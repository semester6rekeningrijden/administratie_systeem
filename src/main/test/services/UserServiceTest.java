package services;

import com.teamterry.administrationsystem.authentication.PermissionEnum;
import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.domain.User;
import com.teamterry.administrationsystem.repositories.interfaces.RoleRepository;
import com.teamterry.administrationsystem.repositories.interfaces.UserRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.RoleService;
import com.teamterry.administrationsystem.services.UserService;
import com.teamterry.administrationsystem.utils.PasswordAuthentication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import sun.security.util.Password;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleService roleService;

    @Mock
    RoleRepository roleRepository;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void UserServiceTest() {
        assertNotNull(userService);
    }

    private List<User> userList() {
        // Arrange
        List<User> list = new ArrayList<>();

        for(int i = 0; i < 5; i++) {
            User u = new User();
            u.setId(i + 1);
            u.setFirstName("firstame" + i);
            u.setLastName("lastname" + i);
            u.setEmail("user" + i + "@mail.com");
            u.setPassword("password" + i);
            u.setStreet("Mystreet" + i);
            u.setCity("Eindhoven");
            u.setCountry("Netherlands");
            u.setZipcode("zipcode" + i);
            list.add(u);
        }

        return list;
    }

    /*
     * Get All Users Test
     */
    @Test
    public void all_StatusCodeOk() {
        // Arrange
        List<User> list = userList();
        when(userRepository.findAll()).thenReturn(list);

        // Act
        ObjectResponse<List<User>> response = userService.all();

        // Assert
        verify(userRepository, atLeastOnce()).findAll();
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(list, response.getObject());
        assertEquals(list.size(), response.getObject().size());
    }

    /*
     * Get Permission By Id Tests
     */
    @Test
    public void getById_ExistingId_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";

        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);
        when(userRepository.findOne(id)).thenReturn(u);

        // Act
        ObjectResponse<User> response = userService.getById(id);

        // Assert
        verify(userRepository, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(u, response.getObject());
    }

    @Test
    public void getById_IdNull_StatusCodeNotAcceptable() {
        // Arange
        int id = 0;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        // Act
        ObjectResponse<User> response = userService.getById(id);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void getById_IdDoesNotExist_StatusCodeNotFound() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);
        when(userRepository.findOne(id)).thenReturn(null);

        // Act
        ObjectResponse<User> response = userService.getById(id);

        // Assert
        verify(userRepository, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Get Permission By Username Tests
     */
    @Test
    public void getByEmail_ExistingEmail_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);
        when(userRepository.findByEmail(email)).thenReturn(u);

        // Act
        ObjectResponse<User> response = userService.getByEmail(email);

        // Assert
        verify(userRepository, atLeastOnce()).findByEmail(email);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(u, response.getObject());
    }

    @Test
    public void getByEmail_EmailNull_StatusCodeNotAcceptable() {
        // Arange
        int id = 866;
        String email = "";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        // Act
        ObjectResponse<User> response = userService.getByEmail(email);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void getByEmail_IdDoesNotExist_StatusCodeNotFound() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);
        when(userRepository.findByEmail(email)).thenReturn(null);

        // Act
        ObjectResponse<User> response = userService.getByEmail(email);

        // Assert
        verify(userRepository, atLeastOnce()).findByEmail(email);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Get Permission By Username Tests
     */
    @Test
    public void getByZipcode_ExistingEmail_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "zipcode1";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        List<User> users = userList();

        List<User> zipcodeUsers = new ArrayList<>();
        zipcodeUsers.add(users.get(1));
        zipcodeUsers.add(u);

        when(userRepository.findByZipcode(zipcode)).thenReturn(zipcodeUsers);

        // Act
        ObjectResponse<List<User>> response = userService.getByZipcode(zipcode);

        // Assert
        verify(userRepository, atLeastOnce()).findByZipcode(zipcode);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(zipcodeUsers, response.getObject());
    }

    /*
     * Get Permission By Username Tests
     */
    @Test
    public void getByZipcode_ZipcodeDoesNotExist_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "zipcode1";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        when(userRepository.findByZipcode(zipcode)).thenReturn(null);

        // Act
        ObjectResponse<List<User>> response = userService.getByZipcode(zipcode);

        // Assert
        verify(userRepository, atLeastOnce()).findByZipcode(zipcode);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Create User Tests
     */
    @Test
    public void create_newWithProperItems_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "zipcode1";
        User u = new User();
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        when(userRepository.findByEmail(email)).thenReturn(null);
        when(roleService.findByName(roleName)).thenReturn(new ObjectResponse<>(HttpCode.OK, String.format("Role with id %s and name %s found", role.getId(), role.getName()), role));
        when(userRepository.save(u)).thenReturn(u);

        // Act
        ObjectResponse<User> response = userService.create(u);

        // Assert
        verify(userRepository, atLeastOnce()).findByEmail(email);
        verify(roleService, atLeastOnce()).findByName(roleName);
        verify(userRepository, atLeastOnce()).save(u);
        assertEquals(HttpCode.CREATED, response.getCode());
        assertEquals(u, response.getObject());
    }

    @Test
    public void create_newWithProperItemsAndUnknownRole_StatusCodeNotFound() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "zipcode1";
        User u = new User();
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        when(userRepository.findByEmail(email)).thenReturn(null);
        when(roleService.findByName(roleName)).thenReturn(new ObjectResponse<>(HttpCode.NOT_FOUND, String.format("Role with name %s not found", role.getName())));

        // Act
        ObjectResponse<User> response = userService.create(u);

        // Assert
        verify(userRepository, atLeastOnce()).findByEmail(email);
        verify(roleService, atLeastOnce()).findByName(roleName);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void create_EmailNull_StatusCodeNotAcceptable() {
        // Arange
        int id = 6;
        String email = "";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "zipcode1";
        User u = new User();
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        // Act
        ObjectResponse<User> response = userService.create(u);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void create_ZipcodeNull_StatusCodeNotAcceptable() {
        // Arange
        int id = 6;
        String email = "thisismymail@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "";
        User u = new User();
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        // Act
        ObjectResponse<User> response = userService.create(u);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void create_newWithExistingEmail_StatusCodeConflict() {
        // Arange
        int existingId = 6;

        int id = 6;
        String email = "thisismymail@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "myzipcode";
        User u = new User();
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);


        User u2 = new User();
        u2.setId(existingId);
        u2.setFirstName(firstname);
        u2.setLastName(lastname);
        u2.setEmail(email);
        u2.setPassword(password);
        u2.setStreet(street);
        u2.setCity(city);
        u2.setCountry(country);
        u2.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        when(userRepository.findByEmail(email)).thenReturn(u2);

        // Act
        ObjectResponse<User> response = userService.create(u);

        // Assert
        verify(userRepository, atLeastOnce()).findByEmail(email);
        assertEquals(HttpCode.CONFLICT, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Update User Tests
     */
    @Test
    public void update_newWithProperItems_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "thisismymail@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "myzipcode";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        when(userRepository.findOne(id)).thenReturn(u);
        when(userRepository.findByEmail(email)).thenReturn(null);
        when(userRepository.save(u)).thenReturn(u);

        // Acts
        ObjectResponse<User> response = userService.update(u);

        // Assert
        verify(userRepository, atLeastOnce()).findOne(id);
        verify(userRepository, atLeastOnce()).findByEmail(email);
        verify(userRepository, atLeastOnce()).save(u);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(u, response.getObject());
    }

    @Test
    public void update_emailNull_StatusCodeNotAcceptable() {
        // Arange
        int id = 6;
        String email = "";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "myzipcode";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        String roleName = "employee";

        Role role = new Role();
        role.setId(id);
        role.setName(roleName);

        // Acts
        ObjectResponse<User> response = userService.update(u);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Delete User Tests
     */
    @Test
    public void delete_ExistingId_StatusCodeOk() {
        // Arange
        int id = 6;
        String email = "";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "myzipcode";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);
        when(userRepository.findOne(id)).thenReturn(u);

        // Act
        ObjectResponse<User> response = userService.delete(u);

        // Assert
        verify(userRepository, atLeastOnce()).findOne(id);
        verify(userRepository, atLeastOnce()).delete(u);
        assertEquals(HttpCode.OK, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void delete_IdNull_StatusCodeNotAcceptable() {
        // Arange
        int id = 0;
        String email = "";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "myzipcode";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        // Act
        ObjectResponse<User> response = userService.delete(u);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void delete_IdNDoesNotExist_StatusCodeNotFound() {
        // Arange
        int id = 6;
        String email = "";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "myzipcode";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);
        when(userRepository.findOne(id)).thenReturn(null);

        // Act
        ObjectResponse<User> response = userService.delete(u);

        // Assert
        verify(userRepository, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void hasPermissions_OnePermission_StatusOk() {
        // Arange
        int id = 6;
        String email = "testUser@mail.com";
        String password = "password123";
        String firstname = "FirstName";
        String lastname = "LaStNaMe";
        String street = "myStreeT";
        String city = "Eindhoven";
        String country = "Netherlands";
        String zipcode = "5551AA";
        User u = new User();
        u.setId(id);
        u.setFirstName(firstname);
        u.setLastName(lastname);
        u.setEmail(email);
        u.setPassword(password);
        u.setStreet(street);
        u.setCity(city);
        u.setCountry(country);
        u.setZipcode(zipcode);

        Role role = new Role("employee");
        u.setRole(role);

        List<Permission> permissions = new ArrayList<>();
        permissions.add(new Permission(PermissionEnum.READ_PERMISSIONS.getValue()));
        permissions.add(new Permission(PermissionEnum.CREATE_PERMISSIONS.getValue()));
        permissions.add(new Permission(PermissionEnum.UPDATE_PERMISSIONS.getValue()));
        permissions.add(new Permission(PermissionEnum.DELETE_PERMISSIONS.getValue()));

        role.setPermissions(permissions);
        String permissionString = PermissionEnum.READ_PERMISSIONS.getValue();


        // Act
        ObjectResponse<Boolean> response = userService.hasPermission(u, permissionString);

        // Assert
        assertEquals(HttpCode.OK, response.getCode());
        assertTrue(response.getObject());
    }
}
