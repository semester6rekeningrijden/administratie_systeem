package services;

import com.teamterry.administrationsystem.domain.Permission;
import com.teamterry.administrationsystem.domain.Role;
import com.teamterry.administrationsystem.repositories.interfaces.RoleRepository;
import com.teamterry.administrationsystem.responses.HttpCode;
import com.teamterry.administrationsystem.responses.ObjectResponse;
import com.teamterry.administrationsystem.services.PermissionService;
import com.teamterry.administrationsystem.services.RoleService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceTest {

    @InjectMocks
    private RoleService roleService;

    @Mock
    private RoleRepository rr;

    @Mock
    private PermissionService pr;

    @Before
    public void setUp() {
        initMocks(this);
    }

    /*
     * Get All Permissions Test
     */
    @Test
    public void all_StatusCodeOk() {
        // Arrange
        List<Role> roleList = new ArrayList<>();
        roleList.add(new Role("Admin"));
        roleList.add(new Role("Member"));
        roleList.add(new Role("Moderator"));
        when(rr.findAll()).thenReturn(roleList);

        // Act
        ObjectResponse<List<Role>> response = roleService.all();

        // Assert
        verify(rr, atLeastOnce()).findAll();
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(roleList, response.getObject());
        assertEquals(roleList.size(), response.getObject().size());
    }


    /*
     * Get Permission By Id Tests
     */
    @Test
    public void getById_ExistingId_StatusCodeOk() {
        // Arange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findOne(id)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.findById(id);

        // Assert
        verify(rr, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(role, response.getObject());
    }

    @Test
    public void getById_IdNull_StatusCodeNotAceptable() {
        // Arange
        int id = 0;

        // Act
        ObjectResponse<Role> response = roleService.findById(id);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void getById_NotExistingId_StatusCodeNotFound() {
        // Arange
        int id = 6;
        when(rr.findOne(id)).thenReturn(null);

        // Act
        ObjectResponse<Role> response = roleService.findById(id);

        // Assert
        verify(rr, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Get Permission By Name Tests
     */
    @Test
    public void getByName_ExistingName_StatusCodeOk() {
        // Arange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findByName(name)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.findByName(name);

        // Assert
        verify(rr, atLeastOnce()).findByName(name);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(role, response.getObject());
    }

    @Test
    public void getByName_NoName_StatusCodeNotAcceptable() {
        // Arange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);

        // Act
        ObjectResponse<Role> response = roleService.findByName("");

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void getByName_NotExistingName_StatusCodeNotFound() {
        // Arange
        String name = "Admin";

        // Act
        ObjectResponse<Role> response = roleService.findByName(name);

        // Assert
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }



    /*
     * Create Permission Tests
     */
    @Test
    public void create_NewWithName_StatusCodeCreated() {
        // Arrange
        String name = "Admin";
        Role role = new Role(name);
        when(rr.save(role)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.create(role);

        // Assert
        verify(rr, atLeastOnce()).save(role);
        assertEquals(HttpCode.CREATED, response.getCode());
        assertEquals(role.getName(), response.getObject().getName());
    }

    @Test
    public void create_NewWithoutName_StatusCodeNotAcceptable() {
        // Arrange
        String name = "";
        Role role = new Role(name);

        // Act
        ObjectResponse<Role> response = roleService.create(role);

        // Assert
        verify(rr, never()).save(role);
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void create_NewWithDuplicateName_StatusCodeConflict() {
        // Arrange
        String name = "Admin";
        Role role = new Role(name);

        when(rr.findByName(name)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.create(role);

        // Assert
        verify(rr, atLeastOnce()).findByName(name);
        verify(rr, never()).save(role);
        assertEquals(HttpCode.CONFLICT, response.getCode());
        assertNull(response.getObject());
    }


    /*
     * Update Permission Tests
     */
    @Test
    public void update_ExistingWithNewName_StatusCodeOk() {
        //Arange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findOne(id)).thenReturn(role);
        when(rr.findByName(name)).thenReturn(null);
        when(rr.save(role)).thenReturn(role);

        //Act
        ObjectResponse<Role> response = roleService.update(role);

        //Assert
        verify(rr, atLeastOnce()).findOne(id);
        verify(rr, atLeastOnce()).findByName(name);
        verify(rr, atLeastOnce()).save(role);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(role.getName(), response.getObject().getName());
    }

    @Test
    public void update_ExistingWithDuplicateName_StatusCodeConflict() {
        //Arange
        String name = "Admin";

        int existingId = 654;
        Role existingRole = new Role();
        existingRole.setId(existingId);
        existingRole.setName(name);

        int id = 6;

        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findOne(id)).thenReturn(role);
        when(rr.findByName(name)).thenReturn(existingRole);

        //Act
        ObjectResponse<Role> response = roleService.update(role);

        //Assert
        verify(rr, atLeastOnce()).findOne(id);
        verify(rr, atLeastOnce()).findByName(name);
        assertEquals(HttpCode.CONFLICT, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void update_withNoName_StatusCodeNotAcceptable() {
        //Arange
        int id = 6;
        String name = "";
        Role role = new Role();
        role.setId(id);
        role.setName(name);

        //Act
        ObjectResponse<Role> response = roleService.update(role);

        //Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void update_withIdNull_StatusCodeNotAcceptable() {
        //Arange
        int id = 0;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);

        //Act
        ObjectResponse<Role> response = roleService.update(role);

        //Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void update_IdDoesNotExist_StatusCodeOk() {
        //Arange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findOne(id)).thenReturn(null);

        //Act
        ObjectResponse<Role> response = roleService.update(role);

        //Assert
        verify(rr, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Delete Permission Tests
     */
    @Test
    public void delete_ExistingId_StatusCodeOk() {
        // Arrange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findOne(id)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.delete(role);

        // Assert
        verify(rr, atLeastOnce()).findOne(id);
        verify(rr, atLeastOnce()).delete(role);
        assertEquals(HttpCode.OK, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void delete_IdNull_StatusCodeNotAcceptable() {
        // Arrange
        int id = 0;
        String name = "permission name";
        Role role = new Role();
        role.setId(id);
        role.setName(name);

        // Act
        ObjectResponse<Role> response = roleService.delete(role);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void delete_IdDoesNotExist_StatusCodeNotFound() {
        // Arrange
        int id = 6;
        String name = "Admin";
        Role role = new Role();
        role.setId(id);
        role.setName(name);
        when(rr.findOne(id)).thenReturn(null);

        // Act
        ObjectResponse<Role> response = roleService.delete(role);

        // Assert
        verify(rr, atLeastOnce()).findOne(id);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Add Permission Tests
     */
    @Test
    public void addPermission_withExistingRoleAndPermission_StatusCodeOk() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        when(rr.findOne(roleId)).thenReturn(role);
        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.OK, "Permission with name: " + permission.getName() + " found", permission));
        when(rr.attachPermission(role, permission)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.attachPermission(role, permission);

        // Assert
        verify(rr, atLeastOnce()).findOne(roleId);
        verify(pr, atLeastOnce()).findById(permissionId);
        verify(rr, atLeastOnce()).attachPermission(role, permission);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(role, response.getObject());
    }

    @Test
    public void addPermission_RoleIdNull_StatusCodeNotAcceptable() {
        // Arange
        int roleId = 0;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        // Act
        ObjectResponse<Role> response = roleService.attachPermission(role, permission);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void addPermission_PermissionIdNull_StatusCodeNotAcceptable() {
        // Arange
        int roleId = 654;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 0;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        // Act
        ObjectResponse<Role> response = roleService.attachPermission(role, permission);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void addPermission_PermissionDoesNotExist_StatusCodeOk() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.NOT_FOUND, "Permission not found"));

        // Act
        ObjectResponse<Role> response = roleService.attachPermission(role, permission);

        // Assert
        verify(pr, atLeastOnce()).findById(permissionId);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void addPermission_RoleDoesNotExist_StatusCodeOk() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        when(rr.findOne(roleId)).thenReturn(null);
        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.OK, "Permission with name: " + permission.getName() + " found", permission));

        // Act
        ObjectResponse<Role> response = roleService.attachPermission(role, permission);

        // Assert
        verify(rr, atLeastOnce()).findOne(roleId);
        verify(pr, atLeastOnce()).findById(permissionId);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void addPermission_RoleAndPermissionAlreadyAdded_StatusCodeOk() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        role.attachPermission(permission);

        when(rr.findOne(roleId)).thenReturn(role);
        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.OK, "Permission with name: " + permission.getName() + " found", permission));

        // Act
        ObjectResponse<Role> response = roleService.attachPermission(role, permission);

        // Assert
        verify(rr, atLeastOnce()).findOne(roleId);
        verify(pr, atLeastOnce()).findById(permissionId);
        assertEquals(HttpCode.FORBIDDEN, response.getCode());
        assertNull(response.getObject());
    }

    /*
     * Remove Permission Tests
     */
    @Test
    public void removePermission_withExistingRoleAndPermission_StatusCodeOk() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        role.attachPermission(permission);

        when(rr.findOne(roleId)).thenReturn(role);
        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.OK, "Permission with name: " + permission.getName() + " found", permission));
        when(rr.detachPermission(role, permission)).thenReturn(role);

        // Act
        ObjectResponse<Role> response = roleService.detachPermission(role, permission);

        // Assert
        verify(rr, atLeastOnce()).findOne(roleId);
        verify(pr, atLeastOnce()).findById(permissionId);
        verify(rr, atLeastOnce()).detachPermission(role, permission);
        assertEquals(HttpCode.OK, response.getCode());
        assertEquals(role, response.getObject());
    }

    @Test
    public void removePermission_RoleIdNull_StatusCodeNotAcceptable() {
        // Arange
        int roleId = 0;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        role.attachPermission(permission);

        // Act
        ObjectResponse<Role> response = roleService.detachPermission(role, permission);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void removePermission_PermissionIdNull_StatusCodeNotAcceptable() {
        // Arange
        int roleId = 654;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 0;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        role.attachPermission(permission);

        // Act
        ObjectResponse<Role> response = roleService.detachPermission(role, permission);

        // Assert
        assertEquals(HttpCode.NOT_ACCEPTABLE, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void removePermission_PermissionDoesNotExist_StatusCodeNotFound() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        role.attachPermission(permission);

        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.NOT_FOUND, "Permission not found"));

        // Act
        ObjectResponse<Role> response = roleService.detachPermission(role, permission);

        // Assert
        verify(pr, atLeastOnce()).findById(permissionId);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void removePermission_RoleDoesNotExist_StatusCodeNotFound() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        role.attachPermission(permission);

        when(rr.findOne(roleId)).thenReturn(null);
        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.OK, "Permission with name: " + permission.getName() + " found", permission));

        // Act
        ObjectResponse<Role> response = roleService.detachPermission(role, permission);

        // Assert
        verify(rr, atLeastOnce()).findOne(roleId);
        verify(pr, atLeastOnce()).findById(permissionId);
        assertEquals(HttpCode.NOT_FOUND, response.getCode());
        assertNull(response.getObject());
    }

    @Test
    public void removePermission_RoleAndPermissionAreNotAdded_StatusCodeForbidden() {
        // Arange
        int roleId = 6;
        String roleName = "Admin";
        Role role = new Role();
        role.setId(roleId);
        role.setName(roleName);

        int permissionId = 563;
        String permissionName = "create_tweet";
        Permission permission = new Permission();
        permission.setId(permissionId);
        permission.setName(permissionName);

        when(rr.findOne(roleId)).thenReturn(role);
        when(pr.findById(permissionId)).thenReturn(new ObjectResponse<>(HttpCode.OK, "Permission with name: " + permission.getName() + " found", permission));

        // Act
        ObjectResponse<Role> response = roleService.detachPermission(role, permission);

        // Assert
        verify(rr, atLeastOnce()).findOne(roleId);
        verify(pr, atLeastOnce()).findById(permissionId);
        assertEquals(HttpCode.FORBIDDEN, response.getCode());
        assertNull(response.getObject());
    }
}
