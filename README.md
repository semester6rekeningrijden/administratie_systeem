# Rekeningrijden

## Payara Realm
In order to get payara realm to function properly, you'll have to create a view in your database in advance.

```sql
CREATE VIEW v_user_role AS SELECT u.id AS userId, r.id as RoleId, u.email, r.name FROM user AS u INNER JOIN role AS r ON u.role_id = r.id
```
 
In payara create a new Realm:
- name: e.g `jdbcRealm`
- classname: `com.sun.enterprise.security.auth.realm.jdbc.JDBCRealm`
- JAAS Context: jdbcRealm
- JNDI: (database jndi) e.g `jdbc/registrationsystem`
- user Table: `user`
- username column: `email`
- password column: `password`
- group table: `v_user_role` (the view we created)
- group table username column: `email`
- group Name column: `name`

